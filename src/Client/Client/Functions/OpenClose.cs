using System.Xml.Linq;
using System.IO;
using System;

using HarkLib.Core;
using HarkLib.Security;

namespace Hark.Cloud.Client
{
    public partial class Client
    {
        private void Close()
        {
            Session.Send(new XElement("disconnect"));

            if(IsStoredCloudSession && File.Exists(TemporaryFile))
                File.Delete(TemporaryFile);
            
            Console.WriteLine(" [o] Session closed.");
        }

        private CloudSession OpenSession(string sTimeout = null, string sTotalTimeout = null)
        {
            int timeout = 2 * 60 * 60; // 2h
            if(sTimeout != null && !int.TryParse(sTimeout, out timeout))
                throw new FormatException("Can't parse the timeout.");
            
            int totalTimeout = 0; // infinite
            if(sTotalTimeout != null && !int.TryParse(sTotalTimeout, out totalTimeout))
                throw new FormatException("Can't parse the total timeout.");

            Console.Write("Cloud IP/Port : ");
            this.CloudIpPort = CoreHelper.ParseIPEndPoint(Console.ReadLine());

            Console.Write("Username : ");
            string username = Console.ReadLine();

            Console.Write("Password : ");
            byte[] password = SecureConsole.ReadSecurePassword().Bytes;

            string sNbIterations;
            int nbIterations;
            do
            {
                Console.Write("Security level (nb hash - min 10000) [10000] : ");
                sNbIterations = Console.ReadLine().Trim();
                if(sNbIterations.Length == 0)
                {
                    nbIterations = 10000;
                    break;
                }
            } while(!int.TryParse(sNbIterations, out nbIterations) || nbIterations < 10000);

            CloudClient cc = new CloudClient(CloudIpPort);
            return cc.OpenSession(username, password, nbIterations, timeout, totalTimeout);
        }

        private void OpenD(string sTimeout = null, string sTotalTimeout = null)
        {
            Console.WriteLine(OpenSession(sTimeout, sTotalTimeout).ToString());
        }

        private void Open(string sTimeout = null, string sTotalTimeout = null)
        {
            File.WriteAllText(TemporaryFile, OpenSession(sTimeout, sTotalTimeout).ToString());
            Console.WriteLine(" [o] Session opened.");
        }
    }
}
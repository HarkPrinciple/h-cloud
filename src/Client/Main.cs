using System;

using HarkLib.Core;

namespace Hark.Cloud.Client
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                new Client().Compute(args);
            }
            catch(CloudException ex)
            {
                Console.Error.WriteLine(" [!] {0} - {1}", ex.Code, ex.Message);
            }
            catch(System.Net.Sockets.SocketException)
            {
                Console.Error.WriteLine(" [!] Couldn't send or receive to/from the server.");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                Console.Error.WriteLine(" [!] {0}", ex.Message);
            }
        }
    }
}
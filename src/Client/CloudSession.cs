using System.Net.Sockets;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System.IO;
using System;

using HarkLib.Core;
using HarkLib.Security;

namespace Hark.Cloud.Client
{
    public class CloudSession
    {
        public CloudSession(
            string sessionId,
            byte[] key,
            byte[] iv,
            CloudClient cloudClient)
        {
            this.CloudClient = cloudClient;
            this.SessionId = sessionId;
            this.Key = key;
            this.IV = iv;
        }
        public CloudSession(string sessionString)
        {
            string[] strings = sessionString.Split("@");

            this.CloudClient = new CloudClient(CoreHelper.ParseIPEndPoint(strings[0].Trim()));
            this.SessionId = strings[1].Trim();
            this.Key = Convert.FromBase64String(strings[2].Trim());
            this.IV = Convert.FromBase64String(strings[3].Trim());
        }
        public CloudSession(string ipPort, string sessionStringWithoutIpPort)
            : this(ipPort.Trim() + "@" + sessionStringWithoutIpPort.Trim())
        { }

        public override string ToString()
        {
            return CloudClient.CloudIpPort.ToString() + "@"
                + SessionId + "@"
                + Convert.ToBase64String(Key) + "@"
                + Convert.ToBase64String(IV);
        }

        public CloudClient CloudClient
        {
            get;
            private set;
        }

        public string SessionId
        {
            get;
            private set;
        }

        private byte[] Key
        {
            get;
            set;
        }

        private byte[] IV
        {
            get;
            set;
        }

        private bool _LongTimeout = false;
        private int oldTimeout;
        public bool LongTimeout
        {
            get
            {
                return _LongTimeout;
            }
            set
            {
                if(value != _LongTimeout)
                {
                    _LongTimeout = value;

                    if(value)
                    {
                        oldTimeout = CloudClient.Server.Client.ReceiveTimeout;
                        CloudClient.Server.Client.ReceiveTimeout = 60000;
                    }
                    else
                    {
                        CloudClient.Server.Client.ReceiveTimeout = oldTimeout;
                    }
                }
            }
        }

        private static int sMsgId = 0;

        public XElement Send(XElement msg)
        {
            return Send(msg.ToString(SaveOptions.DisableFormatting));
        }
        public XElement Send(string msg)
        {
            return Send(Encoding.UTF8.GetBytes(msg));
        }
        public XElement Send(byte[] msg)
        {
            int msgId = ++sMsgId;

            XElement doc = new XElement("secure",
                new XElement("messageid", msgId),
                new XElement("sessionid", SessionId),
                new XElement("content", Convert.ToBase64String(AES.Encrypt(msg, Key, IV)))
            );
            byte[] data = Encoding.UTF8.GetBytes(doc.ToString(SaveOptions.DisableFormatting));

            CloudClient.Server.Send(data, data.Length, CloudClient.CloudIpPort);

            return Receive(msgId);
        }

        public Stream OpenStream(string streamId, string cmd)
        {
            TcpClient client = new TcpClient();
            client.Connect(new IPEndPoint(CloudClient.CloudIpPort.Address, CloudClient.CloudIpPort.Port + 1));
            Stream stream = client.GetStream();

            stream.WriteWrapped(Encoding.UTF8.GetBytes(SessionId));

            stream = new AESStream(stream, Key, IV);

            stream.WriteWrapped(Encoding.UTF8.GetBytes(streamId));
            stream.WriteWrapped(Encoding.UTF8.GetBytes(cmd));
            stream.Flush();

            return stream;
        }

        private bool IsValidId(int? msgId, XElement doc)
        {
            XElement xDocMsgId = doc.XPathSelectElement("/messageid");
            if(msgId != null && xDocMsgId != null)
            {
                xDocMsgId.Remove();
                string docMsgId = xDocMsgId.Value.Trim();
                return docMsgId == msgId.Value.ToString();
            }
            else
                return msgId == null && xDocMsgId == null;
        }
        public XElement Receive(int? msgId = null)
        {
            IPEndPoint remote = new IPEndPoint(IPAddress.Any, 0);
            byte[] data = CloudClient.Server.Receive(ref remote);

            XElement doc;
            try
            {
                doc = XElement.Parse(Encoding.UTF8.GetString(data));
                throw new CloudException(doc);
            }
            catch(System.Xml.XmlException)
            {
                data = AES.Decrypt(data, Key, IV);
                doc = XElement.Parse(Encoding.UTF8.GetString(data));

                if(doc.Name.LocalName.ToLower() == "error")
                    throw new CloudException(doc);

                if(!IsValidId(msgId, doc))
                    return Receive(msgId);
                return doc;
            }
        }
    }
}
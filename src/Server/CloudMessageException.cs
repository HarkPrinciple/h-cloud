using System.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    [System.Serializable]
    public class CloudMessageException : System.Exception
    {
        public CloudMessageException(XElement element)
            : base(element.ToString())
        {
            this.Element = element;
        }
        public CloudMessageException(int code, string message)
            : this(Session.GetError(code, message))
        { }
        public CloudMessageException(string message)
            : this(Session.GetError(0, message))
        { }

        public XElement Element
        {
            get;
            private set;
        }
    }
}
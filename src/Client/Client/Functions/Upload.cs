using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Linq;
using System.IO;
using System;

namespace Hark.Cloud.Client
{
    public partial class Client
    {
        private class ProgressBarInfo
        {
            public double cPercent = 0;
            public double oldPercent = -1.0;
            public string oldString = null;
            public Stopwatch displayProgressSW = null;
            public List<long> progressValues;
            public long oldValue;
            public int cBufferSize = 0;
            public int oldLoadSize = 0;

            public int BufferSize
            {
                get;
                set;
            }

            public void DisplayProgress(double percent, int bufferSize = 0, bool forced = false)
            {
                const int nbPad = 40;

                if(bufferSize == 0)
                    bufferSize = BufferSize;

                double dif = Math.Abs(percent - oldPercent);
                if(/*forced || oldString == null || dif >= 0.000001*/true)
                {
                    if(displayProgressSW == null)
                    {
                        displayProgressSW = new Stopwatch();
                        progressValues = new List<long>();
                        oldValue = 0;
                    }
                    else
                    {
                        cPercent += dif;
                        cBufferSize += bufferSize;
                        displayProgressSW.Stop();
                        if(displayProgressSW.ElapsedMilliseconds >= 1000)
                        {
                            displayProgressSW.Stop();
                            long value = (long)((displayProgressSW.ElapsedMilliseconds * 10000L) / cPercent);
                            progressValues.Add(value);
                            oldValue += value;
                            if(progressValues.Count > 5)
                            {
                                oldValue -= progressValues[0];
                                progressValues.RemoveAt(0);
                            }

                            oldLoadSize = (int)(cBufferSize * (displayProgressSW.ElapsedMilliseconds / 1000)) / 1024;
                            cPercent = 0;
                            cBufferSize = 0;
                            displayProgressSW.Restart();
                        }
                        else
                            displayProgressSW.Start();
                    }

                    oldString = Enumerable
                        .Range(0, (int)(nbPad * percent))
                        .Select(_ => '=')
                        .Aggregate("", (s1,s2) => s1 + s2)
                        .PadRight(nbPad);
                    oldPercent = percent;

                    Console.Write("\r [{0}] {1:P2} | {2} | {3,4} ko/s",
                        oldString,
                        percent,
                        progressValues.Count == 0 ? "?"
                            : new TimeSpan((long)((oldValue / progressValues.Count) * (1.0-percent))).ToString("dd\\.hh\\:mm\\:ss"),
                        oldLoadSize);
                }
            }
        }

        public void Upload(string file, string fileId, bool tcp, int bufferSize = 30000)
        {
            Session.LongTimeout = true;
            string streamId = Session.Send(new XElement("openfile",
                    new XElement("file",
                        new XElement("id", fileId)
                    ),
                    new XElement("mode", "write")
                ))
                .XPathSelectElement("/streamid")
                .Value;
            Session.LongTimeout = false;
            
            Stream stream;

            if(file.Trim() == "-")
                stream = Console.OpenStandardInput();
            else
                stream = new FileStream(file, FileMode.Open);

            if(tcp)
            {
                using(Stream remote = Session.OpenStream(streamId, "write"))
                using(stream)
                {
                    ProgressBarInfo pbi = new ProgressBarInfo();

                    int id = 0;
                    double totalLen = stream.CanSeek ? stream.Length : 0;
                    long nb = 0L;

                    bool ct = true;
                    do
                    {
                        byte[] data = new byte[bufferSize];
                        int len = stream.Read(data, 0, data.Length);

                        if(len <= 0)
                        {
                            ct = false;

                            if(totalLen > 0)
                            {
                                pbi.DisplayProgress(1.0, forced : true);
                                Console.WriteLine();
                            }
                        }
                        else
                        {
                            remote.Write(data, 0, len);
                            if(totalLen > 0)
                            {
                                nb += len;
                                pbi.DisplayProgress(nb / totalLen, len);
                            }
                        }

                        ++id;

                    } while(ct);
                }
            }
            else
            { // UDP
                ProgressBarInfo pbi = new ProgressBarInfo();

                int id = 0;
                using(stream)
                {
                    double totalLen = stream.CanSeek ? stream.Length : 0;
                    long nb = 0L;

                    bool ct = true;
                    do
                    {
                        byte[] data = new byte[bufferSize];
                        int len = stream.Read(data, 0, data.Length);

                        XElement ext;

                        if(len <= 0)
                        {
                            ct = false;
                            ext = new XElement("close");

                            if(totalLen > 0)
                            {
                                pbi.DisplayProgress(1.0, forced : true);
                                Console.WriteLine();
                            }
                        }
                        else
                        {
                            ext = new XElement("write", Convert.ToBase64String(data, 0, len));

                            if(totalLen > 0)
                            {
                                nb += len;
                                pbi.DisplayProgress(nb / totalLen, len);
                            }
                        }

                        while(true)
                        {
                            try
                            {
                                Session.Send(new XElement("writestream",
                                    new XElement("streamid", streamId),
                                    new XElement("packetid", id),
                                    ext
                                ));

                                break;
                            }
                            catch
                            { }
                        }

                        ++id;

                    } while(ct);
                }
            }
        }

        public void Upload(string file, string idXPath, string reserveQuery, bool tcp)
        {
            if(file.Contains("*"))
            {
                DirectoryInfo parent = Directory.GetParent(file);

                if(parent == null)
                    parent = new DirectoryInfo(Directory.GetCurrentDirectory());
                
                Dictionary<string, XElement> queries = new Dictionary<string, XElement>();

                foreach(string f in Directory.GetFileSystemEntries(parent.FullName, Path.GetFileName(file), SearchOption.AllDirectories))
                {
                    Console.WriteLine(" [ ] {0}", Path.GetFileName(f));
                    queries.Add(f, QueryHelper.ConvertQuery(reserveQuery));
                }
                
                foreach(var kv in queries)
                    Upload(kv.Key, idXPath, kv.Value, tcp);
            }
            else
            {
                Console.WriteLine(" [ ] {0}", Path.GetFileName(file));
                Upload(file, idXPath, QueryHelper.ConvertQuery(reserveQuery), tcp);
            }
        }
        public void Upload(string file, string idXPath, XElement reserveQuery, bool tcp)
        {
            if(file.Contains("*"))
            {
                DirectoryInfo parent = Directory.GetParent(file);

                if(parent == null)
                    parent = new DirectoryInfo(Directory.GetCurrentDirectory());
                
                foreach(string f in Directory.GetFileSystemEntries(parent.FullName, Path.GetFileName(file), SearchOption.AllDirectories))
                    Upload(f, idXPath, reserveQuery, tcp);
            }
            else
            {
                Console.WriteLine(" [ ] {0}", Path.GetFileName(file));
                Session.LongTimeout = true;
                string fileId = SendQuery(reserveQuery).XPathSelectElement(idXPath).Value;
                Session.LongTimeout = false;
                Console.WriteLine(" [i] id = {0}", fileId);
                
                Upload(file, fileId, tcp);

                Console.WriteLine(" [o] Uploaded the file.");
            }
        }
    }
}
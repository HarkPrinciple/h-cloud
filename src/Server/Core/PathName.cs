using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using System.Text;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public partial class CloudCore
    {
        public string GetFilePath(string name)
        {
            var toEliminate = new string[]
            {
                "\\", "/", ".", "&", ";", "~"
            };

            foreach(string e in toEliminate)
                name = name.Replace(e, "_");

            return Path.Combine(FilesFolder, "." + name + ".wav");
        }

        public string GetAliasFileName(string name, User user)
        {
            byte[] bname = Hash.HashSHA512(Encoding.UTF8.GetBytes(name), 1);
            byte[] key, iv;
            GetKeyIV(name, user, out key, out iv);

            byte[] result = AES.Encrypt(
                bname,
                key,
                iv
            );

            return result.ToHex().ToUpper();
        }

        public string GetAliasFilePath(string name, User user)
        {
            return GetFilePath(GetAliasFileName(name, user));
        }

        protected string GetFileNameFromPath(string path)
        {
            return Path.GetFileNameWithoutExtension(path).Substring(1);
        }
    }
}
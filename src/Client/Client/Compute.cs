using System.Collections.Generic;
using System.Net.Sockets;
using System.Linq;
using System.IO;
using System;

using HarkLib.Core;

namespace Hark.Cloud.Client
{
    public partial class Client
    {
        public void Compute(string[] args)
        {
            ConsoleManager.Arguments.FromArguments(args)
            .ExtractValue("session", new string[] { "-s", "-session" })
            .ExtractKey("tcp", "-tcp")
            .ExtractKey("tcp", "-udp", false)
            .Before(cas =>
            {
                if(cas.HasValue("session"))
                {
                    string session = cas.GetValue("session").Trim();
                    if(session == "-")
                    {
                        Console.Write("Session : ");
                        session = Console.ReadLine();
                    }
                    this.Session = new CloudSession(session);
                }
            })
            .ExtractCommand("firewall", (_, das) =>
            {
                using(UdpClient client = new UdpClient(new Random().Next(1024, 65535)))
                {
                    Console.WriteLine(" [o] {0} open.", client.Client.LocalEndPoint);
                    Console.WriteLine(" [ ] Closing...");
                }
                Console.WriteLine(" [o] Closed.");
            })
            .ExtractCommand(new string[] { "help", "?" }, (_, das) => DisplayHelp())
            .ExtractCommand("opend/{?timeout}/{?total-timeout}", (_, das) =>
            {
                OpenD(das.GetOr("timeout", null), das.GetOr("total-timeout", null));
            })
            .ExtractCommand("open/{?timeout}/{?total-timeout}", (_, das) =>
            {
                Open(das.GetOr("timeout", null), das.GetOr("total-timeout", null));
            })
            .ExtractCommand("session", (_, das) => Console.WriteLine(Session))
            .ExtractCommand("version", (_, das) => Console.WriteLine(Version))
            .ExtractCommand("update/{?url...}", (_, das) =>
            {
                string updateRoot = das.GetOr("url", "https://bitbucket.org/HarkPrinciple/h-cloud/downloads");
                new Update().Run(new Dictionary<string, string>()
                {
                    { "HarkLib.Security.dll", Path.Combine(updateRoot, "HarkLib.Security.dll") },
                    { "HarkLib.Core.dll", Path.Combine(updateRoot, "HarkLib.Core.dll") },
                    { "HarkCloud.Client.exe", Path.Combine(updateRoot, "HarkCloud.Client.exe") }
                });
            })
            .ExtractCommand("ping", (_, das) => Ping())
            .ExtractCommand("close", (_, das) => Close())
            .ExtractCommand("info/{id}", (_, das) => Info(das["id"]))
            .ExtractCommand("delete/{ids...}", (_, das) =>
            {
                foreach(string id in das["ids"].Split(" "))
                    Delete(id);
            })
            .ExtractCommand("query/{query...}", (_, das) =>
                Query(das["query"]))
            .ExtractCommand("queryget/{id}/{query...}", (_, das) =>
                QueryGet(das["id"], das["query"]))
            .ExtractCommand("admin/claim", (_, das) => ClaimAsAdmin())
            .ExtractCommand("admin/shutdown", (_, das) => Shutdown())
            .ExtractCommand("upload/{files}/{id}", (cas, das) =>
                Upload(das["files"], das["id"], cas.GetKey("tcp")))
            .ExtractCommand("upload/{files}/{id}/{query...}", (cas, das) =>
                Upload(das["files"], das["id"], das["query"], cas.GetKey("tcp")))
            .ExtractCommand("extension/install/{names...}", (_, das) =>
            {
                foreach(string name in das["names"].Split(" "))
                    DownloadExtension(name);
            })
            .ExtractCommand("extension/update", (_, das) =>
                UpdateExtensions())
            .ExtractCommand("extension/uninstall/{names...}", (_, das) =>
            {
                foreach(string name in das["names"].Split(" "))
                    DeleteExtension(name);
            })
            .ExtractCommand("extension/list/{?filter...}", (_, das) =>
                ListExtensions(das.GetOr("filter", null)))
            .ExtractCommand("download/{id}/{?buffer-size}/{?timeout}", (_, das) =>
                Download(das["id"], das.GetOr("buffer-size", null), das.GetOr("timeout", null)))
            .Default(cas =>
            {
                foreach(var kv in Extensions)
                    kv.Value.LoadCommands(this, cas);
                
                cas.Default(_ => DisplayHelp())
                    .Execute();
            })
            .Execute();
        }
    }
}
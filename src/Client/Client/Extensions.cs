using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System;

namespace Hark.Cloud.Client
{
    public partial class Client
    {
        protected Dictionary<string, IClientSessionExtension> LoadAllExtensions(string folderPath)
        {
            var extensions = new Dictionary<string, IClientSessionExtension>();

            if(!Directory.Exists(folderPath))
                return extensions;

            foreach(var f in Directory.GetFiles(folderPath, "*.dll"))
                LoadExtensions(f, extensions);
            return extensions;
        }
        protected void LoadExtensions(string filePath, Dictionary<string, IClientSessionExtension> extensions)
        {
            try
            {
                Type type = Assembly.LoadFrom(filePath).GetType("Hark.Cloud.Extension.ClientExtension");
                if(type != null)
                {
                    IClientSessionExtension cse = (IClientSessionExtension)Activator.CreateInstance(type);
                    extensions.Add(Path.GetFileNameWithoutExtension(filePath), cse);
                }
                else
                    Console.WriteLine(" [!] {0} not found in {1}", "Hark.Cloud.Extension.ClientExtension", filePath);
            }
            catch(Exception ex)
            {
                Console.WriteLine(" [!] Error loading {1} : {0}", ex.Message, filePath);
            }
        }

        private Dictionary<string, IClientSessionExtension> _Extensions = null;
        public Dictionary<string, IClientSessionExtension> Extensions
        {
            get
            {
                if(_Extensions == null)
                    _Extensions = LoadAllExtensions(ExtensionFolder);

                return _Extensions;
            }
        }

        public string ExeFolderLocation
        {
            get
            {
                return Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            }
        }

        public string ExtensionFolderName
        {
            get
            {
                return ".hc_extensions";
            }
        }

        public string ExtensionFolder
        {
            get
            {
                return Path.Combine(ExeFolderLocation, ExtensionFolderName);
            }
        }

        public string GetExtensionPath(string name)
        {
            return Path.Combine(ExtensionFolder, name.Trim().ToLower() + ".dll");
        }
    }
}
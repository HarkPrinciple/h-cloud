using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using System.Text;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public class CloudFile
    {
        private CloudFile(
            CloudCore core,
            User owner,
            string path,
            bool isAlias,
            string originalName)
        {
            this.CloudCore = core;
            this.Owner = owner;
            this.Path = path;
            this.IsAlias = isAlias;
            this.OriginalName = originalName;
        }

        public static CloudFile FromId(string id, User owner, CloudCore core)
        {
            return new CloudFile(
                core : core,
                path : id == null ? null : core.GetFilePath(id),
                owner : owner,
                isAlias : false,
                originalName : id
            );
        }
        public static CloudFile FromAlias(string alias, User owner, CloudCore core, User nameSharing = null)
        {
            string fileName = core.GetAliasFileName(alias, nameSharing ?? owner);

            return new CloudFile(
                core : core,
                path : core.GetFilePath(fileName),
                owner : owner,
                isAlias : true,
                originalName : fileName
            );
        }

        public CloudCore CloudCore
        {
            get;
            private set;
        }

        public bool IsAlias
        {
            get;
            private set;
        }

        public string OriginalName
        {
            get;
            private set;
        }

        public User Owner
        {
            get;
            private set;
        }

        public string Path
        {
            get;
            private set;
        }

        public bool Exists
        {
            get
            {
                return Path != null && File.Exists(Path);
            }
        }
        
        public bool IsOwner(User owner = null)
        {
            return CloudCore.IsOwner(Path, owner ?? Owner);
        }
        
        public void Delete(User owner = null)
        {
            CloudCore.Delete(Path, owner ?? Owner);
        }

        public Stream OpenRead(User owner = null)
        {
            return CloudCore.OpenRead(Path, owner ?? Owner);
        }

        public Stream OpenWrite(User owner = null)
        {
            return CloudCore.OpenWrite(Path, owner ?? Owner);
        }

        public string ReadAll(User owner = null)
        {
            return CloudCore.ReadAll(Path, owner ?? Owner);
        }

        public void WriteAll(string data, User owner = null)
        {
            CloudCore.WriteAll(Path, owner ?? Owner, data);
        }

        public bool Reserve(User owner = null)
        {
            owner = owner ?? Owner;

            if(IsAlias)
            {
                return CloudCore.ReserveFilePath(Path, OriginalName, owner);
            }
            else
            {
                OriginalName = CloudCore.Reserve(owner);
                Path = CloudCore.GetFilePath(OriginalName);
                return true;
            }
        }
    }
}
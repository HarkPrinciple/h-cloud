using System.Collections.Concurrent;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public abstract class IProcessServerSessionExtension : IClaimableServerSessionExtension
    {
        public Process SessionProcess
        {
            get;
            protected set;
        }

        protected XElement Start(Session session, XElement doc)
        {
            if(!IsClaimer(session))
                throw new CloudMessageException("You don't have the right to execute this command.");

            if(SessionProcess != null)
                throw new CloudMessageException("Already started.");
            
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;

            InitializeProcess(startInfo, session, doc);

            SessionProcess = Process.Start(startInfo);

            OnStarted(SessionProcess, session, doc);

            return new XElement("start", new XElement("started"));
        }

        protected XElement Stop(Session session, XElement doc)
        {
            if(!IsClaimer(session))
                throw new CloudMessageException("You don't have the right to execute this command.");

            if(SessionProcess == null)
                throw new CloudMessageException("Web site not started.");

            OnStopping(SessionProcess, session, doc);
            
            SessionProcess.Kill();
            SessionProcess = null;

            return new XElement("stop", new XElement("stoped"));
        }

        protected abstract void InitializeProcess(ProcessStartInfo info, Session session, XElement doc);

        protected virtual void OnStarted(Process process, Session session, XElement doc)
        { }

        protected virtual void OnStopping(Process process, Session session, XElement doc)
        { }
    }
}
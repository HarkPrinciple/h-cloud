using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using System.Text;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public partial class CloudCore
    {
        protected UIDManager UIDManager
        {
            get;
            private set;
        }

        protected Random Random
        {
            get;
            private set;
        }

        public string Reserve(User user)
        {
            string[] names = new string[Random.Next(NbDispatchMin, NbDispatchMax + 1)];
            
            CreateReserve(0, user, ref names);
            
            return names[0];
        }

        private void CreateReserve(int startIndex, User user, ref string[] names)
        {
            for(int i = startIndex; i < names.Length; ++i)
            {
                int outi;
                for(outi = 1000; outi > 0; --outi)
                {
                    names[i] = UIDManager.Reserve().ToString("X");
                    if(!File.Exists(GetFilePath(names[i])))
                        break;
                }
                
                if(outi == 0)
                    throw new Exception("Can't reserve more.");
            }

            AllocateFiles(names, user);
        }

        public bool ReserveAlias(string alias, User user, User owner = null)
        {
            string fileName = GetAliasFileName(alias, user);
            string path = GetFilePath(fileName);
            
            return ReserveFilePath(path, fileName, owner ?? user);
        }

        public bool ReserveFilePath(string path, string fileName, User owner)
        {
            if(File.Exists(path))
                return false;
            
            string[] names = new string[Random.Next(NbDispatchMin, NbDispatchMax + 1)];
            names[0] = fileName;
            CreateReserve(1, owner, ref names);

            return true;
        }
    }
}
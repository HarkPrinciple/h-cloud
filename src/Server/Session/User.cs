using System.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public class User
    {
        public User(string name, byte[] password, int nbIterations)
        {
            this.Name = name;
            this.Password = ProducePassword(password, nbIterations);
            this.NbIterations = nbIterations;
        }

        private static byte[] ProducePassword(byte[] password, int nbIterations)
        {
            byte[] pwdResult = new byte[32];
            byte[] pwd = Hash.HashSHA256(password, nbIterations);
            for(int i = 0; i < 32; ++i)
                pwdResult[i] = (byte)(pwd[i] ^ pwd[i + 32]);
            return pwdResult;
        }

        public string Name
        {
            get;
            private set;
        }
        
        public byte[] Password
        {
            get;
            private set;
        }

        public int NbIterations
        {
            get;
            private set;
        }

        private static volatile User _UniversalUser = null;
        public static User UniversalUser
        {
            get
            {
                if(_UniversalUser == null)
                {
                    _UniversalUser = new User(
                        name : "__universal_user__",
                        password : Encoding.UTF8.GetBytes("__universal_user_password__"),
                        nbIterations : 10000
                    );
                }

                return _UniversalUser;
            }
        }
    }
}
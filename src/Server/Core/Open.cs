using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using System.Text;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public partial class CloudCore
    {
        private Stream Open(string path, User user, FileMode fileMode)
        {
            if(!IsOwner(path, user))
                throw new Exception("You are not the owner of this file.");

            return CreateDispatchStream(path, fileMode, user);
        }

        public Stream OpenRead(string path, User user)
        {
            Stream multiStream = Open(path, user, FileMode.Open);
            multiStream.ReadByte();
            return multiStream;
        }

        public Stream OpenWrite(string path, User user)
        {
            Stream multiStream = Open(path, user, FileMode.Truncate);
            multiStream.WriteByte((byte)Random.Next());
            multiStream.Flush();
            return multiStream;
        }

        public string ReadAll(string path, User user, Encoding encoding = null)
        {
            using(Stream stream = OpenRead(path, user))
            {
                encoding = encoding ?? Encoding.UTF8;
                StringBuilder content = new StringBuilder();
                try
                {
                    while(true)
                    {
                        byte[] buffer = new byte[10024];
                        int len = stream.Read(buffer, 0, buffer.Length);
                        if(len == 0)
                            break;
                        
                        content.Append(encoding.GetString(buffer, 0, len));
                    }
                }
                catch(System.Security.Cryptography.CryptographicException)
                { }

                return content.ToString();
            }
        }

        public void WriteAll(string path, User user, string data, Encoding encoding = null)
        {
            using(Stream stream = OpenWrite(path, user))
            {
                byte[] buffer = (encoding ?? Encoding.UTF8).GetBytes(data);
                stream.Write(buffer);
                stream.Flush();
            }
        }
    }
}
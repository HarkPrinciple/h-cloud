using System.Security.Cryptography;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public class HelpElement
    {
        public HelpElement(
            string name,
            string desc = null,
            string format = null)
        {
            this.Description = desc;
            this.Format = format;
            this.Name = name;
        }

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Format
        {
            get;
            set;
        }

        public override string ToString()
        {
            return ToElement().ToString(SaveOptions.DisableFormatting);
        }

        public XElement ToElement(string name = "command")
        {
            var e = new XElement(name,
                new XElement("name", Name)
            );

            if(!string.IsNullOrEmpty(Description))
                e.Add(new XElement("description", Description.Trim()));

            if(!string.IsNullOrEmpty(Format))
                e.Add(new XElement("format", Format.Trim()));

            return e;
        }

        public static XElement CreateHelpElement(HelpElement[] entries, string name = "help")
        {
            return new XElement(name,
                entries
                    .Select(e => e.ToElement())
                    .ToList()
            );
        }
    }
}
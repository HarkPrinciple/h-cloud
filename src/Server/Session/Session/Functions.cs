using System.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public enum SessionMessage
    {
        None,
        Shutdown,
        Restart
    }

    public partial class Session
    {
        public XElement Compute(IPEndPoint sender, XElement doc, out SessionMessage msg)
        {
            LastRequest = DateTime.Now;
            msg = SessionMessage.None;

            try
            {
                string cmd = doc.Name.LocalName.Trim().ToLower();
                switch(cmd)
                {
                    case "refresh":
                        return new XElement("refresh",
                            new XElement("timeout",
                                new XElement("value", Timeout),
                                new XElement("unit", "second")
                            )
                        );
                    
                    case "ping":
                        return Ping();
                    
                    case "info":
                        return Info(doc);
                    
                    case "reserve":
                        return Reserve(doc);

                    case "delete":
                        return Delete(doc);

                    case "listextensions":
                        return ListExtensions(doc);

                    case "openextension":
                        return OpenExtension(doc);

                    case "openfile":
                        return OpenFile(doc);
                    
                    case "closestream":
                        return CloseStream(doc);
                    
                    case "readstream":
                        return ReadStream(doc);
                    
                    case "writestream":
                        return WriteStream(doc);
                    
                    case "disconnect":
                        return Disconnect();
                    
                    case "admin":
                        return Admin(doc, ref msg);
                    
                    default:
                        SessionExtension sessionExtension;
                        if(Extensions.TryGetValue(cmd, out sessionExtension))
                        {
                            IServerSessionExtension instance;
                            XElement result = sessionExtension.Compute(this, sender, doc, out instance);
                            if(!instance.Threads.IsEmpty)
                                RunningExtensionInstances.Add(instance);
                            return result;
                        }
                        else
                            return GetError(3, "Can't understand the command \"" + cmd + "\".");
                }
            }
            catch(CloudMessageException ex)
            {
                return ex.Element;
            }
            catch(TargetInvocationException ex)
            {
                CloudMessageException cme = ex.InnerException as CloudMessageException;
                if(cme != null)
                    return cme.Element;
                else
                    return GetError(98, ex.InnerException.Message);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return GetError(99, ex.Message);
            }
        }

        private XElement Admin(XElement doc, ref SessionMessage msg)
        {
            XElement shutdown = doc.XPathSelectElement("shutdown");
            XElement restart = doc.XPathSelectElement("restart");
            XElement claim = doc.XPathSelectElement("claim");

            XElement result = new XElement("admin");

            if(claim != null)
            {
                if(!CloudCore.ReserveAlias(AdminClaimName, User.UniversalUser, User))
                    throw new CloudMessageException("Already claimed.");
                
                result.Add(new XElement("claimed"));
            }

            if(shutdown != null)
            {
                ThrowIfNotAdmin();
                result.Add(new XElement("shutdown"));
                msg = SessionMessage.Shutdown;
            }

            if(restart != null)
            {
                ThrowIfNotAdmin();
                result.Add(new XElement("restart"));
                msg = SessionMessage.Restart;
            }

            return result;
        }

        private XElement Ping()
        {
            return new XElement("ping", new XElement("pong"));
        }

        private XElement Info(XElement doc)
        {
            XElement fileTag;
            string filePath = GetFilePath(doc, out fileTag);

            bool fileExists = File.Exists(filePath);

            return new XElement("info",
                new XElement("file", fileTag),
                new XElement("exists", fileExists ? "1" : "0"),
                new XElement("owner", fileExists && CloudCore.IsOwner(filePath, User) ? "1" : "0")
            );
        }

        private XElement Reserve(XElement doc)
        {
            XElement xalias = doc.XPathSelectElement("/alias");
            if(xalias != null)
            {
                string alias = xalias.Value;
                if(!CloudCore.ReserveAlias(alias, User))
                    throw new CloudMessageException(10, "Can't reserve this alias.");

                return new XElement("reserve",
                    new XElement("file", new XElement("alias", alias)),
                    new XElement("reserved")
                );
            }
            else
            {
                string uid = CloudCore.Reserve(User);
                return new XElement("reserve",
                    new XElement("file", new XElement("id", uid)),
                    new XElement("reserved")
                );
            }
        }

        private XElement ListExtensions(XElement doc)
        {
            string[] search = GetOr(doc, "/extension/name", "").Trim().ToLower().Split(" ");
            
            XElement root = new XElement("extensions");

            foreach(var kv in Extensions)
            {
                string name = kv.Key.Trim().ToLower();

                if(search.Length == 0 || search.All(s => name.Contains(s)))
                {
                    XElement element = new XElement("extension",
                        new XElement("name", kv.Key)
                    );
                    
                    if(kv.Value.IsDownloadable)
                        element.Add(new XElement("downloadable"));
                    root.Add(element);
                }
            }

            return root;
        }

        private XElement Delete(XElement doc)
        {
            XElement fileTag;
            string filePath = GetFilePath(doc, out fileTag);
            
            if(!File.Exists(filePath))
                return GetError(5, "Can't find file.");

            CloudCore.Delete(filePath, User);
            
            return new XElement("delete",
                new XElement("file", fileTag),
                new XElement("deleted")
            );
        }

        private void CanOpenNewStream()
        {
            if(Streams.Count >= 100)
                throw new CloudMessageException(20, "Can't open more streams.");
        }

        private XElement OpenExtension(XElement doc)
        {
            CanOpenNewStream();

            string name = GetOrThrow(doc, "/extension/name").Trim().ToLower();

            if(!Extensions.ContainsKey(name))
                return GetError(45, "Can't find extension.");

            var extension = Extensions[name];

            if(!extension.IsDownloadable)
                return GetError(46, "Not downloadable extension.");

            string filePath = extension.FilePath;

            if(!File.Exists(filePath))
                return GetError(45, "Can't find extension.");

            Stream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
    
            int streamid = ++StreamId;
            int buffersize;

            XElement xbuffersize = doc.XPathSelectElement("buffersize");
            if(xbuffersize == null || !int.TryParse(xbuffersize.Value, out buffersize))
                buffersize = 5000;

            Streams.Add(streamid.ToString(), new StreamInfo(stream, buffersize));

            return new XElement("openextension",
                new XElement("extension",
                    new XElement("name", name)
                ),
                new XElement("streamid", streamid),
                new XElement("buffersize", buffersize)
            );
        }

        private XElement OpenFile(XElement doc)
        {
            CanOpenNewStream();
            
            XElement fileTag;
            string filePath = GetFilePath(doc, out fileTag);
            
            string mode = GetOrThrow(doc, "/mode").Trim().ToLower();

            if(!new string[] { "read", "write" }.Contains(mode))
                return GetError(9, "Wrong value in tag \"mode\".");

            if(mode == "read" && !File.Exists(filePath))
                return GetError(5, "Can't find file.");

            Stream stream = mode == "read" ? CloudCore.OpenRead(filePath, User) : CloudCore.OpenWrite(filePath, User);
    
            int streamid = ++StreamId;
            int buffersize;

            XElement xbuffersize = doc.XPathSelectElement("buffersize");
            if(xbuffersize == null || !int.TryParse(xbuffersize.Value, out buffersize))
                buffersize = 5000;

            Streams.Add(streamid.ToString(), new StreamInfo(stream, buffersize));

            return new XElement("openfile",
                new XElement("file", fileTag),
                new XElement("streamid", streamid),
                new XElement("buffersize", buffersize)
            );
        }

        private XElement CloseStream(XElement doc)
        {
            string sid = GetOrThrow(doc, "/streamid");

            Stream stream = Streams[sid].Stream;
            Streams.Remove(sid);
            stream.Close();

            return new XElement("closestream", new XElement("closed"));
        }

        private XElement ReadStream(XElement doc)
        {
            string sid = GetOrThrow(doc, "/streamid");
            StreamInfo streamInfo = Streams[sid];

            XElement xresend = doc.XPathSelectElement("/resend");
            if(xresend != null)
            {
                return new XElement("readstream",
                    new XElement("data", Convert.ToBase64String(streamInfo.OldData, 0, streamInfo.OldLen))
                );
            }

            Stream stream = streamInfo.Stream;

            byte[] data = streamInfo.OldData;
            streamInfo.OldLen = stream.Read(data, 0, data.Length);

            if(streamInfo.OldLen == 0)
            {
                Streams.Remove(sid);
                stream.Close();
                return new XElement("readstream", new XElement("closed"));
            }

            return new XElement("readstream",
                new XElement("data", Convert.ToBase64String(data, 0, streamInfo.OldLen))
            );
        }

        private XElement WriteStream(XElement doc)
        {
            string sid = GetOrThrow(doc, "/streamid");
            StreamInfo streamInfo = Streams[sid];
            Stream stream = streamInfo.Stream;

            List<XElement> vs = new List<XElement>();

            XElement xdata = doc.XPathSelectElement("write");
            if(xdata != null)
            {
                XElement packetId = doc.XPathSelectElement("packetid");
                if(packetId == null || packetId.Value.Trim() != streamInfo.PacketId)
                {
                    if(packetId != null)
                        streamInfo.PacketId = packetId.Value.Trim();
                    
                    byte[] data = Convert.FromBase64String(xdata.Value);
                    stream.Write(data, 0, data.Length);
                    stream.Flush();

                    vs.Add(new XElement("write", new XElement("repeated")));
                }
                else
                {
                    vs.Add(new XElement("write"));
                }
            }

            if(doc.XPathSelectElement("close") != null)
            {
                Streams.Remove(sid);
                stream.Close();
                vs.Add(new XElement("closed"));
            }

            return new XElement("writestream", vs);
        }

        private XElement Disconnect()
        {
            this.Alive = false;

            foreach(var str in Streams.Values)
                str.Stream.Close();
            Streams.Clear();

            return new XElement("disconnect",
                new XElement("code", 1),
                new XElement("message", "Disconnected with success.")
            );
        }
    }
}
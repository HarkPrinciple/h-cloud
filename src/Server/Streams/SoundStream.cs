using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public class SoundStream : BasedStream
    {
        public SoundStream(Stream stream)
            : base(AsSound(stream))
        { }

        public override int Read(byte[] data, int start, int len)
        {
            return BaseStream.Read(data, start, len);
        }
        public override void Write(byte[] data, int start, int len)
        {
            BaseStream.Write(data, start, len);
        }

        public override void Close()
        {
            int size = (int)BaseStream.Length;

            BaseStream.Position = 4;
            BaseStream.Write((size - 8).GetBytes());
            BaseStream.Flush();

            BaseStream.Position = 40;
            BaseStream.Write((size - 44).GetBytes());
            BaseStream.Flush();

            BaseStream.Close();
        }
        
        /// <summary>
        /// Create the sound stream from the dialog stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <note>
        /// See : http://m8051.blogspot.fr/2010/08/wave-sound-file-in-pcm-format-long-lost.html"/>
        /// </note>
        /// <returns>Stream representing the sound.</returns>
        public static Stream AsSound(Stream stream)
        {
            const short nbChannels = 2;
            const int sampleRate = 44100;
            const short bitsPerSample = 8;

            stream.Write("RIFF".GetBytes());
            stream.Write(((int)0).GetBytes()); // ?
            stream.Write("WAVE".GetBytes());
            stream.Write("fmt ".GetBytes());
            stream.Write(((int)16).GetBytes()); // PCM
            stream.Write(((short)1).GetBytes()); // = PCM
            stream.Write(nbChannels.GetBytes()); // nbChannels (1=mono; 2=stereo; ...)
            stream.Write(sampleRate.GetBytes()); // sampleRate
            stream.Write((sampleRate * nbChannels * bitsPerSample / 8).GetBytes()); // byteRate = sampleRate * nbChannels * bitsPerSample/8
            stream.Write(((short)(nbChannels * bitsPerSample / 8)).GetBytes()); // blockAlign = nbChannels * bitsPerSample/8
            stream.Write(bitsPerSample.GetBytes()); // bitsPerSample
            stream.Write("data".GetBytes());
            stream.Write(((int)0).GetBytes()); // ? - subchunkSize = nbSamples * nbChannels * bitsPerSample/8

            return stream;
        }

        public static Stream SkipSoundHeader(Stream stream)
        {
            stream.Read(44);
            return stream;
        }
    }
}
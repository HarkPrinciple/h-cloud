using System.Collections.Generic;
using System;

using HarkLib.Core;

namespace Hark.Cloud.Client
{
    public abstract class IClientSessionExtension : MarshalByRefObject
    {
        public abstract IDictionary<string, string> Help
        {
            get;
        }

        public abstract void LoadCommands(Hark.Cloud.Client.Client client, ConsoleManager.Arguments args);
    }
}
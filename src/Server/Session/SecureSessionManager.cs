using System.Collections.Concurrent;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public class SecureSessionManager
    {
        public SecureSessionManager(ConsoleManager.Arguments configuration)
        {
            this.KeysTimeout = new ConcurrentDictionary<string, DateTime>();
            this.SessionId = 0;
            this.CloudCore = new CloudCore(configuration);
            this.Sessions = new ConcurrentDictionary<string, Session>();
            this.Keys = new ConcurrentDictionary<string, RSAKeys>();

            string extDirectory = configuration.GetValue("Extensions-Folder");
            if(Directory.Exists(extDirectory))
                this.Extensions = LoadAllExtensions(extDirectory);
            else
                this.Extensions = new ConcurrentDictionary<string, SessionExtension>();
        }

        public CloudCore CloudCore
        {
            get;
            private set;
        }

        protected IDictionary<string, Session> Sessions
        {
            get;
            private set;
        }

        private int SessionId
        {
            get;
            set;
        }

        private IDictionary<string, RSAKeys> Keys
        {
            get;
            set;
        }
        private IDictionary<string, DateTime> KeysTimeout
        {
            get;
            set;
        }

        protected XElement Init()
        {
            RSAKeys rsaKeys = new RSAKeys();
            string sexp = Convert.ToBase64String(rsaKeys.PublicKey.Exponent);
            string smod = Convert.ToBase64String(rsaKeys.PublicKey.Modulus);
            string rsaKey = sexp + smod;

            DateTime now = DateTime.Now;

            KeysTimeout
                .Where(kv => now > kv.Value.AddSeconds(10))
                .ToList()
                .Select(kv => kv.Key)
                .ForEach(k =>
                {
                    Keys.Remove(k);
                    KeysTimeout.Remove(k);
                });

            Keys.Add(rsaKey, rsaKeys);
            KeysTimeout.Add(rsaKey, now);
            
            return new XElement("init",
                new XElement("id", rsaKey),
                new XElement("rsa",
                    new XElement("exp", sexp),
                    new XElement("mod", smod)
                )
            );
        }

        public IDictionary<string, SessionExtension> Extensions
        {
            get;
            private set;
        }
        public IDictionary<string, SessionExtension> LoadAllExtensions(string folderPath)
        {
            var extensions = new ConcurrentDictionary<string, SessionExtension>();
            foreach(var f in Directory.GetFiles(folderPath, "*.dll"))
                LoadExtension(f, extensions);
            return extensions;
        }
        public void LoadExtension(string filePath, IDictionary<string, SessionExtension> extensions)
        {
            try
            {
                Console.WriteLine(" [ ] Loading {0} ...", filePath);
                Type type = Assembly.LoadFrom(filePath).GetType("Hark.Cloud.Extension.ServerExtension");
                if(type != null)
                {
                    SessionExtension se = new SessionExtension(type, filePath);
                    extensions[se.CommandName] = se;

                    Console.WriteLine(" [o] Loaded {0} [{1}]", se.CommandName, filePath);
                }
                else
                    Console.WriteLine(" [!] {0} not found in {1}", "Hark.Cloud.Extension.ServerExtension", filePath);
            }
            catch(TargetInvocationException ex)
            {
                Console.WriteLine(" [!] Error loading {1} : {0}", ex.InnerException.Message, filePath);
                Console.WriteLine(ex.InnerException); // for debug purpose
            }
            catch(Exception ex)
            {
                Console.WriteLine(" [!] Error loading {1} : {0}", ex.Message, filePath);
            }
        }

        protected byte[] Connect(XElement doc)
        {
            string rsaId = Session.GetOrThrow(doc, "/id");

            RSAKeys rsaKeys = Keys[rsaId];
            
            string id = (++SessionId).ToString();

            byte[] aesKey = HarkLib.Security.RSA.Decrypt(
                Convert.FromBase64String(
                    Session.GetOrThrow(doc, "/aes/key")
                ), rsaKeys
            );
            byte[] aesIV = HarkLib.Security.RSA.Decrypt(
                Convert.FromBase64String(
                    Session.GetOrThrow(doc, "/aes/iv")
                ), rsaKeys
            );

            int nbIterations;
            if(!int.TryParse(Session.GetOrThrow(doc, "/user/nbiterations"), out nbIterations))
            {
                return Encoding.UTF8.GetBytes(
                    Session.GetError(7, "Wrong format for the tag \"nbiterations\".")
                    .ToString(SaveOptions.DisableFormatting)
                );
            }
            User user = new User(
                name : Encoding.UTF8.GetString(
                    HarkLib.Security.RSA.Decrypt(
                        Convert.FromBase64String(
                            Session.GetOrThrow(doc, "/user/name")),
                    rsaKeys)),
                password : HarkLib.Security.RSA.Decrypt(
                    Convert.FromBase64String(
                        Session.GetOrThrow(doc, "/user/password")),
                    rsaKeys),
                nbIterations : nbIterations
            );

            int timeout = int.Parse(Session.GetOr(doc, "/timeout/sleep", (2 * 60 * 60).ToString()));
            int globalTimeout = int.Parse(Session.GetOr(doc, "/timeout/total", "0"));

            Sessions.Add(id, new Session(CloudCore, aesKey, aesIV, user, timeout, globalTimeout, Extensions));

            Keys.Remove(rsaId);
            KeysTimeout.Remove(rsaId);
            
            return AES.Encrypt(
                Encoding.UTF8.GetBytes(
                    new XElement("connect",
                        new XElement("sessionid", id)
                    ).ToString(SaveOptions.DisableFormatting)
                ), aesKey, aesIV);
        }

        protected byte[] Secure(XElement doc, IPEndPoint sender)
        {
            string id = Session.GetOrThrow(doc, "/sessionid");
            string msgId = Session.GetOr(doc, "/messageid", null);

            Session session;

            if(Sessions.TryGetValue(id, out session))
            {
                doc = XElement.Parse(
                    Encoding.UTF8.GetString(
                        HarkLib.Security.AES.Decrypt(
                            Convert.FromBase64String(
                                Session.GetOrThrow(doc, "/content")),
                            session.AESKey,
                            session.AESIV
                        )
                    )
                );

                SessionMessage sessionMsg;
                XElement msg = session.Compute(sender, doc, out sessionMsg);
                if(msgId != null)
                    msg.Add(new XElement("messageid", msgId));

                byte[] result = AES.Encrypt(
                    Encoding.UTF8.GetBytes(
                        msg
                        .ToString(SaveOptions.DisableFormatting)
                    ),
                    session.AESKey,
                    session.AESIV
                );
                
                if(!session.Alive)
                    Sessions.Remove(id);
                
                switch(sessionMsg)
                {
                    case SessionMessage.Shutdown:
                        foreach(var kv in Sessions)
                            kv.Value.Dispose();
                        
                        Environment.Exit(0);
                        break;
                }
                
                return result;
            }
            else
            { // Can't find the session
                XElement msg = Session.GetError(4, "Can't find the session.");

                if(msgId != null)
                    msg.Add(new XElement("messageid", msgId));

                return Encoding.UTF8.GetBytes(
                    msg.ToString(SaveOptions.DisableFormatting)
                );
            }
        }

        public int SessionWipeTime
        {
            get
            {
                return 5 * 60; // 5 minutes
            }
        }

        public DateTime LastSessionWipe
        {
            get;
            private set;
        }

        public bool IsTimeToWipeSessions
        {
            get
            {
                return DateTime.Now >= LastSessionWipe.AddSeconds(SessionWipeTime);
            }
        }

        public byte[] RouteRequest(IPEndPoint sender, string data)
        {
            if(IsTimeToWipeSessions)
            {
                LastSessionWipe = DateTime.Now;
                var outdatedSessions = Sessions
                    .Where(s => s.Value != null)
                    .Where(s => s.Value.TimedOut || !s.Value.Alive)
                    .ToList();
                
                foreach(var session in outdatedSessions)
                {
                    session.Value.Dispose();
                    Sessions.Remove(session.Key);
                }
            }

            try
            {
                XElement doc = XElement.Parse(data);

                switch(doc.Name.LocalName.Trim().ToLower())
                {
                    case "init":
                        return Encoding.UTF8.GetBytes(Init().ToString(SaveOptions.DisableFormatting));

                    case "connect":
                        return Connect(doc);

                    case "secure":
                        return Secure(doc, sender);
                    
                    default:
                        return Encoding.UTF8.GetBytes(
                            Session
                            .GetError(8, "Command unrecognized.")
                            .ToString(SaveOptions.DisableFormatting)
                        );
                }
            }
            catch(Exception ex)
            {
                return Encoding.UTF8.GetBytes(ex.Message);
            }
        }

        public void RouteStream(Stream stream)
        {
            try
            {
                string sessionId = Encoding.UTF8.GetString(stream.ReadWrapped());

                Session session;

                if(Sessions.TryGetValue(sessionId, out session))
                {
                    using(stream = new AESStream(stream, session.AESKey, session.AESIV))
                    {
                        string streamId = Encoding.UTF8.GetString(stream.ReadWrapped());
                        string cmd = Encoding.UTF8.GetString(stream.ReadWrapped());

                        StreamInfo si = session.Streams[streamId];

                        using(Stream fileStream = si.Stream)
                        {
                            switch(cmd)
                            {
                                case "write":
                                    stream.CopyTo(fileStream);
                                    break;

                                case "read":
                                    fileStream.CopyTo(stream);
                                    break;
                            }
                        }

                        session.Streams.Remove(streamId);
                    }
                }
            }
            catch
            { }
        }
    }
}
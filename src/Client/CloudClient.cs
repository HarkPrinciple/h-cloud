using System.Security.Cryptography;
using System.Net.Sockets;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System;

using HarkLib.Security;

namespace Hark.Cloud.Client
{
    public class CloudClient
    {
        public CloudClient(IPEndPoint cloudIpPort)
        {
            Server = new UdpClient();
            Server.Client.ReceiveTimeout = 3000;

            try
            {
                uint SIO_UDP_CONNRESET = 0x80000000 | 0x18000000 | 12;
                Server.Client.IOControl((int)SIO_UDP_CONNRESET, new byte[] { Convert.ToByte(false) }, null);
            }
            catch(System.Net.Sockets.SocketException)
            { }

            this.CloudIpPort = cloudIpPort;
        }

        public IPEndPoint CloudIpPort
        {
            get;
            protected set;
        }

        public UdpClient Server
        {
            get;
            protected set;
        }

        private static string OpenSession_RSA(string data, RSAParameters rsap)
        {
            return OpenSession_RSA(Encoding.UTF8.GetBytes(data), rsap);
        }
        private static string OpenSession_RSA(byte[] data, RSAParameters rsap)
        {
            return Convert.ToBase64String(HarkLib.Security.RSA.Encrypt(data, rsap));
        }
        public CloudSession OpenSession(
            string username,
            byte[] password,
            int nbiterations,
            int timeout,
            int totalTimeout)
        {
            return OpenSession(
                Encoding.UTF8.GetBytes(username),
                password,
                nbiterations,
                timeout,
                totalTimeout
            );
        }
        public CloudSession OpenSession(
            byte[] username,
            string password,
            int nbiterations,
            int timeout,
            int totalTimeout)
        {
            return OpenSession(
                username,
                Encoding.UTF8.GetBytes(password),
                nbiterations,
                timeout,
                totalTimeout
            );
        }
        public CloudSession OpenSession(
            string username,
            string password,
            int nbiterations,
            int timeout,
            int totalTimeout)
        {
            return OpenSession(
                Encoding.UTF8.GetBytes(username),
                Encoding.UTF8.GetBytes(password),
                nbiterations,
                timeout,
                totalTimeout
            );
        }
        public CloudSession OpenSession(
            byte[] username,
            byte[] password,
            int nbiterations,
            int timeout,
            int totalTimeout)
        {
            int readTimeout = Server.Client.ReceiveTimeout;

            // [i] timeout : 1 minute
            //     RSA keys generation is a slow process for the server
            //     Password hash may be slow
            Server.Client.ReceiveTimeout = 60000;
            
            XElement doc = SendRaw(new XElement("init"));
            string rsaId = doc.XPathSelectElement("/id").Value;

            RSAParameters rsap = new RSAParameters()
            {
                Exponent = Convert.FromBase64String(doc.XPathSelectElement("/rsa/exp").Value),
                Modulus = Convert.FromBase64String(doc.XPathSelectElement("/rsa/mod").Value),
                InverseQ = null,
                DP = null,
                DQ = null,
                D = null,
                P = null,
                Q = null
            };

            byte[] key;
            byte[] iv;
            AES.ProduceKeyIV(out key, out iv);
            
            doc = new XElement("connect",
                new XElement("id", rsaId),
                new XElement("user",
                    new XElement("name", OpenSession_RSA(username, rsap)),
                    new XElement("password", OpenSession_RSA(password, rsap)),
                    new XElement("nbiterations", nbiterations)
                ),
                new XElement("timeout",
                    new XElement("sleep", timeout),
                    new XElement("total", totalTimeout)
                ),
                new XElement("aes",
                    new XElement("key", OpenSession_RSA(key, rsap)),
                    new XElement("iv", OpenSession_RSA(iv, rsap))
                )
            );
            
            byte[] data = AES.Decrypt(SendRawGetBytes(doc), key, iv);
            doc = XElement.Parse(Encoding.UTF8.GetString(data));
            string sessionId = doc.XPathSelectElement("/sessionid").Value;

            Server.Client.ReceiveTimeout = readTimeout;

            return new CloudSession(sessionId, key, iv, this);

        }

        public XElement SendRaw(XElement msg)
        {
            return SendRaw(msg.ToString(SaveOptions.DisableFormatting));
        }
        public XElement SendRaw(string msg)
        {
            return SendRaw(Encoding.UTF8.GetBytes(msg));
        }
        public XElement SendRaw(byte[] msg)
        {
            byte[] data = SendRawGetBytes(msg);

            XElement element = XElement.Parse(Encoding.UTF8.GetString(data));
            if(element.Name.LocalName.Trim().ToLower() == "error")
                throw new CloudException(element);

            return element;
        }

        public byte[] SendRawGetBytes(XElement msg)
        {
            return SendRawGetBytes(msg.ToString(SaveOptions.DisableFormatting));
        }
        public byte[] SendRawGetBytes(string msg)
        {
            return SendRawGetBytes(Encoding.UTF8.GetBytes(msg));
        }
        public byte[] SendRawGetBytes(byte[] msg)
        {
            Server.Send(msg, msg.Length, CloudIpPort);

            IPEndPoint remote = new IPEndPoint(IPAddress.Any, 0);
            byte[] data = Server.Receive(ref remote);

            return data;
        }
    }
}
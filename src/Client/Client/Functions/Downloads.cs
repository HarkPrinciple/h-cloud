using System.Net.Sockets;
using System.Xml.XPath;
using System.Xml.Linq;
using System.IO;
using System;

namespace Hark.Cloud.Client
{
    public partial class Client
    {
        public void Download(XElement request, string streamIdXPath, Func<Stream> streamProvider, int receiveTimeout = 2000)
        {
            Download(request, streamIdXPath, _ => streamProvider(), receiveTimeout);
        }
        public void Download(XElement request, string streamIdXPath, Func<XElement, Stream> streamProvider, int receiveTimeout = 2000)
        {
            Session.LongTimeout = true;
            XElement response = Session.Send(request);
            Session.LongTimeout = false;
            string streamId = response.XPathSelectElement(streamIdXPath).Value;

            XElement read = new XElement("readstream",
                new XElement("streamid", streamId)
            );
            XElement readAgain = new XElement("readstream",
                new XElement("streamid", streamId),
                new XElement("resend")
            );
            
            ReceiveTimeout = receiveTimeout;

            using(Stream stream = streamProvider(response))
            {
                while(true)
                {
                    XElement elem = read;
                    while(true)
                    {
                        try
                        {
                            elem = Session.Send(elem);
                            break;
                        }
                        catch(SocketException)
                        {
                            Console.Error.WriteLine(" [!] Reception timed out.");
                            if(elem == read)
                                elem = readAgain;
                        }
                    }

                    if(elem.XPathSelectElement("/closed") != null)
                        break;

                    string value = elem.XPathSelectElement("/data").Value;
                    byte[] data = Convert.FromBase64String(value);

                    if(data.Length == 0)
                    {
                        Console.Error.WriteLine(" [!] No data received.");
                        continue;
                    }
                    
                    stream.Write(data, 0, data.Length);
                    stream.Flush();
                }
            }
        }

        public void Download(string fileId, string bufferSize, string receiveTimeout = null)
        {
            Download(
                fileId : fileId,
                bufferSize : int.Parse(bufferSize ?? "10000"),
                receiveTimeout : int.Parse(receiveTimeout ?? "2000")
            );
        }
        public void Download(string fileId)
        {
            Download(fileId, 10000, 2000);
        }
        public void Download(string fileId, int bufferSize)
        {
            Download(fileId, bufferSize, 2000);
        }
        public void Download(string fileId, int bufferSize, int receiveTimeout)
        {
            if(fileId.Trim() == "-")
                fileId = Console.ReadLine();

            Download(
                request : new XElement("openfile",
                    new XElement("file",
                        new XElement("id", fileId)
                    ),
                    new XElement("mode", "read"),
                    new XElement("buffersize", bufferSize)
                ),
                streamIdXPath : "/streamid",
                streamProvider : Console.OpenStandardOutput,
                receiveTimeout : receiveTimeout
            );
        }
    }
}
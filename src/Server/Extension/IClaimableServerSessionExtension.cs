using System.Collections.Concurrent;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public abstract class IClaimableServerSessionExtension : IServerSessionExtension
    {
        public override bool IsStateful
        {
            get
            {
                return true;
            }
        }

        protected abstract string ClaimName
        {
            get;
        }

        protected virtual bool IsClaimer(Session session)
        {
            CloudFile cf = session.CloudFileFromAlias(ClaimName, User.UniversalUser);
            if(!cf.Exists)
                return false;
            
            return cf.IsOwner();
        }

        protected virtual void ThrowIfNotClaimer(Session session, string message = null)
        {
            if(!IsClaimer(session))
                throw new CloudMessageException(message ?? "You don't have the right to execute this command.");
        }

        protected virtual XElement Claim(Session session, string responseRootTagName = null)
        {
            CloudFile cf = session.CloudFileFromAlias(ClaimName, User.UniversalUser);
            if(!cf.Reserve())
                throw new CloudMessageException("Already claimed.");
            
            return new XElement(responseRootTagName ?? "claim", new XElement("claimed"));
        }
    }
}
using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using System.Text;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public partial class CloudCore
    {
        public CloudCore(ConsoleManager.Arguments configuration)
        {
            this.NbDispatchSpan = 2;
            this.NbDispatchMin = 3;
            this.Configuration = configuration;
            this.FilesFolder = configuration.GetValue("Files-Path");
            this.UIDManager = new RandomUIDManager();
            this.Random = Processor.CreateRandom();

            if(!Directory.Exists(FilesFolder))
                Directory.CreateDirectory(FilesFolder);
        }

        public ConsoleManager.Arguments Configuration
        {
            get;
            private set;
        }

        public string FilesFolder
        {
            get;
            private set;
        }
        
        public bool IsOwner(string path, User user)
        {
            try
            {
                using(Stream multiStream = CreateDispatchStream(path, FileMode.Open, user))
                {
                    multiStream.ReadByte();
                }
                
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }
        
        public void Delete(string path, User user)
        {
            if(!IsOwner(path, user))
                throw new Exception("You are not the owner of this file.");

            foreach(string name in GetNames(path, user))
                File.Delete(GetFilePath(path));
        }
    }
}
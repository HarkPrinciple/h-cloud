using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using System;

namespace Hark.Cloud.Client
{
    public static class QueryHelper
    {
        public static string ExtractQuery(string[] args, int startIndex)
        {
            return args.Skip(startIndex).Aggregate("", (s1, s2) => s1 + " " + s2).Trim();
        }

        public static XElement ConvertQuery(string[] args, int startIndex)
        {
            return ConvertQuery(ExtractQuery(args, startIndex));
        }
        public static XElement ConvertQuery(string query)
        {
            IDictionary<string, string> values = new Dictionary<string, string>();

            Regex rex = new Regex("{{ *([a-zA-Z0-9_-]+) *}}");
            foreach(Match m in rex.Matches(query))
            {
                string name = m.Groups[1].Value;
                if(values.ContainsKey(name))
                    continue;
                
                Console.Write(" [?] {0} : ", name.Trim());
                values.Add(name, Console.ReadLine());
            }

            foreach(var kv in values)
                query = new Regex("{{ *" + kv.Key + " *}}")
                    .Replace(query, kv.Value);

            XElement root = null;
            XElement current = null;
            
            rex = new Regex("/?(?<name>[a-zA-Z0-9]+)(=(?<value>[^;/]*))?(?<opBack>;+)?");
            foreach(Match m in rex.Matches(query))
            {
                string opBack = m.Groups["opBack"].Value;
                string name = m.Groups["name"].Value;
                var value = m.Groups["value"];

                XElement oldCurrent = current;
                if(value.Success)
                    current = new XElement(name, value.Value);
                else
                    current = new XElement(name);

                if(oldCurrent != null)
                    oldCurrent.Add(current);

                if(root == null)
                    root = current;

                for(int i = 0; i < opBack.Length; ++i)
                    current = current.Parent;
            }

            return root;
        }
    }
}
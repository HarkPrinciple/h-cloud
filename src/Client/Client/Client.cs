using System.Net;
using System.IO;
using System;

namespace Hark.Cloud.Client
{
    public partial class Client
    {
        public IPEndPoint CloudIpPort
        {
            set;
            get;
        }
        
        private string TemporaryFile
        {
            get
            {
                return Path.Combine(Path.GetTempPath(), "_cloud_client_session_");
            }
        }

        public int ReceiveTimeout
        {
            get
            {
                return Session.CloudClient.Server.Client.ReceiveTimeout;
            }
            set
            {
                Session.CloudClient.Server.Client.ReceiveTimeout = value;
            }
        }

        private CloudSession _Session = null;
        public CloudSession Session
        {
            get
            {
                if(_Session == null)
                {
                    if(!File.Exists(TemporaryFile))
                        throw new Exception("No session temporary stored.");
                    
                    _IsStoredCloudSession = true;
                    _Session = new CloudSession(File.ReadAllText(TemporaryFile));
                }

                return _Session;
            }
            set
            {
                _IsStoredCloudSession = false;
                _Session = value;
            }
        }

        private bool _IsStoredCloudSession = false;
        public bool IsStoredCloudSession
        {
            get
            {
                var _ = Session;
                return _IsStoredCloudSession;
            }
            private set
            {
                _IsStoredCloudSession = value;
            }
        }

        public string Version
        {
            get
            {
                return "1.0.0";
            }
        }
    }
}
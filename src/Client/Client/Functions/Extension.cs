using System.Net.Sockets;
using System.Xml.XPath;
using System.Xml.Linq;
using System.IO;
using System;

namespace Hark.Cloud.Client
{
    public partial class Client
    {
        public void ListExtensions(string searchPattern = null)
        {
            searchPattern = searchPattern ?? "";

            if(searchPattern.Trim() == "-")
                searchPattern = Console.ReadLine();
            searchPattern = searchPattern.Trim().ToLower();

            XElement msg = new XElement("listextensions");

            if(searchPattern.Length > 0)
                msg.Add(new XElement("extension", new XElement("name", searchPattern)));
            
            Console.WriteLine("========[ Legend ]========");
            Console.WriteLine(" [ ] : Not downloadable");
            Console.WriteLine(" [-] : Downloadable");
            Console.WriteLine(" [o] : Downloaded");
            Console.WriteLine("=========[ List ]=========");
            
            foreach(var e in Session.Send(msg).Elements())
            {
                string name = e.XPathSelectElement("name").Value.Trim();
                Console.WriteLine(" [{1}] {0}",
                    name,
                    File.Exists(GetExtensionPath(name)) ? "o" :
                        e.XPathSelectElement("downloadable") != null ? "-" : " "
                );
            }
        }

        public void UpdateExtensions()
        {
            foreach(string file in Directory.GetFiles(ExtensionFolder, "*.dll"))
                DownloadExtension(Path.GetFileNameWithoutExtension(file));

            Console.WriteLine(" [o] All extensions updated.");
        }

        public void DownloadExtension(string extensionName)
        {
            if(extensionName.Trim() == "-")
                extensionName = Console.ReadLine();
            extensionName = extensionName.Trim().ToLower();

            try
            {
                Download(
                    request : new XElement("openextension",
                        new XElement("extension",
                            new XElement("name", extensionName)
                        )
                    ),
                    streamIdXPath : "/streamid",
                    streamProvider : () =>
                    {
                        if(!Directory.Exists(ExtensionFolder))
                            Directory.CreateDirectory(ExtensionFolder);
                        return File.Open(GetExtensionPath(extensionName), FileMode.Create);
                    }
                );
            }
            catch(NullReferenceException)
            {
                Console.WriteLine(" [!] Can't find the extension {0}.", extensionName);
                return;
            }

            Console.WriteLine(" [o] Extension {0} downloaded.", extensionName);
        }
        
        public void DeleteExtension(string extensionName)
        {
            if(extensionName.Trim() == "-")
                extensionName = Console.ReadLine();
            extensionName = extensionName.Trim().ToLower();

            string extPath = GetExtensionPath(extensionName);

            if(!File.Exists(extPath))
            {
                Console.Error.WriteLine(" [!] Can't find the extension {0}.", extensionName);
                return;
            }

            File.Delete(extPath);
            Console.WriteLine(" [o] Extension {0} deleted.", extensionName);
        }
    }
}
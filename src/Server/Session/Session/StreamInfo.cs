using System.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public class StreamInfo
    {
        public StreamInfo(Stream stream, int bufferSize)
        {
            this.PacketId = null;
            this.OldData = new byte[bufferSize];
            this.Stream = stream;
            this.OldLen = 0;
        }

        public int BufferSize
        {
            get
            {
                return OldData.Length;
            }
        }

        public string PacketId
        {
            get;
            set;
        }

        public Stream Stream
        {
            get;
            private set;
        }

        public byte[] OldData
        {
            get;
            set;
        }

        public int OldLen
        {
            get;
            set;
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using System;

using HarkLib.Core;

namespace Hark.Cloud.Client
{
    public partial class Client
    {
        public IDictionary<string, string> Help
        {
            get
            {
                return new Dictionary<string, string>()
                {
                    { "? / help", "Display this help" },
                    { "version", "Display the version of the client" },
                    { "opend [<timeout> [<total-timeout>]]", "Open a session and display the session string" },
                    { "open [<timeout> [<total-timeout>]]", "Open a session" },
                    { "close", "Close a session" },
                    { "session", "Display the current session" },
                    { "ping", "Check if the session is still usable" },
                    { "info <id>", "Get information about the file" },
                    { "delete <id...>", "Delete one or many files" },
                    { "query <query:qxml>", "Send a query to the server" },
                    { "queryget <selector:xpath> <query:qxml>", "Send a query and display the content of a tag" },
                    { "upload <files:wc> <cid:xpath> <reserve:qxml>", "Reserve and upload file(s)" },
                    { "upload <file> <cid>", "Upload a file" },
                    { "download <id> [<buffer-size> [<timeout>]]", "Download a file and display to the std::out" },
                    { "extension install <name...>", "Install an extension" },
                    { "          uninstall <name...>", "Uninstall an extension" },
                    { "          update", "Update the installed extensions" },
                    { "          list [<name-filter>]", "List remote extensions" },
                    { "admin claim", "Claim the cloud" },
                    { "      shutdown", "Shutdown the cloud" },
                    { "update", "Update the client core" },
                    { "--firewall", "Force to prompt the firewall of Windows" },
                };
            }
        }

        public bool DisplayHelp(string[] args, int nb)
        {
            if(args.Length < nb)
            {
                DisplayHelp();
                return true;
            }
            else
                return false;
        }

        public void DisplayHelp()
        {
            var values = new Dictionary<string, IDictionary<string, string>>();

            values.Add("Standard commands", this.Help);
            foreach(var kv in Extensions)
            {
                if(kv.Value.Help != null && kv.Value.Help.Count > 0)
                    values.Add(kv.Key, kv.Value.Help);
            }

            ConsoleManager.DisplayHelp(
                version : this.Version,
                values : values,
                usages : new string[]
                {
                    "[~<session>] <Command> [<Args...>]",
                    "[~<ip:port@sessionId@rsaExp@rsaMod>] <Command> [<Args...>]",
                    "~192.168.0.36:65500@1@iLg6JyiQufbWu3otOrY6e3dqKCUWqJC0mjbeE3HsoII=@LQUacXrIRJWx7nRLZhDc+w== info 5",
                    "info 5"
                },
                legends : new Dictionary<string, IDictionary<string, string>>()
                {
                    { "Types", new Dictionary<string, string>()
                        {
                            { "wc", "Wildcard (/bin/*.sh)" },
                            { "xpath", "XPath (/parent/child/id)" },
                            { "qxml", "Query XML (/parent/child/id=12;name=Super;;child2/name={{name}})" },
                            { "-", "Standard input" }
                        }
                    }
                },
                leftSize : 44
            );
        }
    }
}
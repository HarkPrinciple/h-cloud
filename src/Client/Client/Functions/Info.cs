using System.Xml.XPath;
using System.Xml.Linq;
using System;

namespace Hark.Cloud.Client
{
    public partial class Client
    {
        public void Info(string fileId)
        {
            if(fileId.Trim() == "-")
                fileId = Console.ReadLine();

            XElement doc = Session.Send(new XElement("info",
                new XElement("file",
                    new XElement("id", fileId)
                )
            ));

            Console.WriteLine(" [-] File : {0}", doc.XPathSelectElement("file/id").Value);

            if(doc.XPathSelectElement("exists").Value == "1")
                Console.WriteLine(" [o] Exists");
            else
                Console.WriteLine(" [x] Doesn't exist");

            if(doc.XPathSelectElement("owner").Value == "1")
                Console.WriteLine(" [o] Owned");
            else
                Console.WriteLine(" [x] Not owned");
        }

        public void Delete(string fileId)
        {
            if(fileId.Trim() == "-")
                fileId = Console.ReadLine();

            Session.Send(new XElement("delete",
                new XElement("file",
                    new XElement("id", fileId)
                )
            ));

            Console.WriteLine(" [o] Deleted file {0}.", fileId);
        }

        public void Ping()
        {
            Console.WriteLine(" [ ] Ping sent.");
            Session.Send(new XElement("ping"));
            Console.WriteLine(" [o] Ping received.");
            Console.WriteLine(" [o] Session checked.");
        }
    }
}
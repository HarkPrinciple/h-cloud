using System.Xml.XPath;
using System.Xml.Linq;
using System;

namespace Hark.Cloud.Client
{
    public partial class Client
    {
        public XElement SendQuery(string query)
        {
            return SendQuery(QueryHelper.ConvertQuery(query));
        }
        public XElement SendQuery(XElement query)
        {
            return Session.Send(query);
        }

        public void Query(string query)
        {
            Console.WriteLine(SendQuery(query));
        }

        public void QueryGet(string get, string query)
        {
            Console.WriteLine(SendQuery(query).XPathSelectElement(get).Value);
        }
    }
}
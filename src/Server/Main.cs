using System.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public static class Program
    {
        public static string Version
        {
            get
            {
                return "1.0.0";
            }
        }

        public static bool DisplayHelp(List<string> args, int nbMin)
        {
            if(args.Count < nbMin)
            {
                DisplayHelp();
                return true;
            }

            return false;
        }
        public static void DisplayHelp()
        {
            ConsoleManager.DisplayHelp(
                version : Version,
                values : new Dictionary<string, IDictionary<string, string>>()
                {
                    { "Commands", new Dictionary<string, string>()
                        {
                            { "update [<remote-folder:url>]", "Update the server core" },
                            { "start", "Start the server" },
                            { "extension install <name...>", "Install one or many extensions" },
                            { "          uninstall <name...>", "Uninstall one or many extensions" },
                            { "          list", "List all known extensions" },
                            { "          update", "Update all known installed extensions" }
                        }
                    },
                    { "Global options", new Dictionary<string, string>()
                        {
                            { "-ip <ip>:<port>", "Filter the clients (0.0.0.0:<port> = all clients)" },
                            { "-f / -folder <folder-path>", "Cloud files folder" },
                            { "-ex / -extension <folder-path>", "Extensions folder" },
                            { "-cf / -configure <file-path>", "Configuration file" },
                            { "-repo <url>", "Repository URL"}
                        }
                    }
                },
                usages : new String[]
                {
                    "<cmd> [<args>]",
                    "start -ip <ip>:<port> -f <folder>",
                    "start -ip 0.0.0.0:18888 -f /mnt/cloud",
                    "start -cf .cloud.ini"
                }
            );
        }

        public static void Main(string[] args)
        {
            ConsoleManager.Arguments.FromArguments(args)
            .ExtractValue("Extensions-Folder", new string[] { "-ex", "-extension" }, ".hc_extensions")
            .ExtractValue("Files-Path", new string[] { "-f", "-folder" })
            .ExtractValue("Cloud-IP", "-ip", "127.0.0.1:65000")
            .ExtractValue("Configuration-File", new string[] { "-cf", "-configure" })
            .ExtractValue("Repository-List", "-repo", "https://bitbucket.org/HarkPrinciple/h-cloud/downloads/extensions.ini")
            .Before(cas =>
            {
                if(cas.HasValue("Configuration-File"))
                {
                    FileSettings fs = new FileSettings(cas.GetValue("Configuration-File"));
                    foreach(var kv in fs.Map)
                        cas.ArgumentsKeyValue[kv.Key] = kv.Value;
                }

                string ext = cas.GetValue("Extensions-Folder");
                if(!Directory.Exists(ext) && !Path.IsPathRooted(ext))
                    cas.ArgumentsKeyValue["Extensions-Folder"] = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), ext);
            })
            .ExtractCommand("update/{?url...}", (cas, das) =>
            {
                string updateRoot = das.GetOr("url", "https://bitbucket.org/HarkPrinciple/h-cloud/downloads");
                new Update().Run(new Dictionary<string, string>()
                {
                    { "HarkLib.Security.dll", Path.Combine(updateRoot, "HarkLib.Security.dll") },
                    { "HarkLib.Core.dll", Path.Combine(updateRoot, "HarkLib.Core.dll") },
                    { "HarkCloud.exe", Path.Combine(updateRoot, "HarkCloud.exe") }
                });
            })
            .ExtractCommand("extension/install/{names...}", (cas, das) =>
                InstallExtensions(das["names"].Split(" "), cas))
            .ExtractCommand("extension/uninstall/{names...}", (cas, das) =>
                UninstallExtensions(das["names"].Split(" "), cas))
            .ExtractCommand("extension/update", (cas, das) =>
                UpdateExtensions(cas))
            .ExtractCommand("extension/list", (cas, das) =>
                ListExtensions(cas))
            .ExtractCommand("start", (cas, das) =>
                Start(cas))
            .Default(cas => DisplayHelp())
            .Execute();
        }

        public static void UpdateExtensions(ConsoleManager.Arguments args)
        {
            string[] files = new DirectoryInfo(args.GetValue("Extensions-Folder"))
                .GetFiles("*.dll")
                .Select(f => f.Name)
                .Select(f => Path.GetFileNameWithoutExtension(f))
                .ToArray();

            if(files.Length == 0)
            {
                Console.WriteLine(" [i] There is no extension installed.");
                return;
            }

            InstallExtensions(files, args);
        }

        public static void ListExtensions(ConsoleManager.Arguments args)
        {
            using(var client = new WebClient())
            {
                var officialExtensions = FileSettings.Parse(client.DownloadString(args.GetValue("Repository-List")));

                foreach(var kv in officialExtensions)
                {
                    if(!kv.Key.EndsWith("___name") && kv.Key != "USER" && kv.Key != "HOME")
                    {
                        string name;
                        if(!officialExtensions.TryGetValue(kv.Key + "___name", out name))
                            name = kv.Key;
                        string dest = Path.Combine(args.GetValue("Extensions-Folder"), name.ToLower() + ".dll");
                        
                        if(name != kv.Key)
                            Console.WriteLine(" [{2}] {0} ({1})", kv.Key, name, File.Exists(dest) ? "o" : " ");
                        else
                            Console.WriteLine(" [{1}] {0}", kv.Key, File.Exists(dest) ? "o" : " ");
                    }
                }
            }
        }

        public static void UninstallExtensions(
            string[] names,
            ConsoleManager.Arguments args)
        {
            if(!Directory.Exists(args.GetValue("Extensions-Folder")))
            {
                Console.Error.WriteLine(" [-] Nothing to uninstall.");
                return;
            }
            
            Dictionary<string, string> officialExtensions = null;

            foreach(string aname in names)
            {
                string name;
                if(aname == "-")
                {
                    Console.Write(" [?] Extension name to uninstall : ");
                    name = Console.ReadLine();
                }
                else
                    name = aname;

                Console.Error.WriteLine(" [ ] Uninstalling {0}...", name);
                try
                {
                    string lowName = name.ToLower();
                    string fileName;

                    using(var client = new WebClient())
                    {
                        Uri result;
                        if(Uri.TryCreate(name, UriKind.Absolute, out result) && (result.Scheme == Uri.UriSchemeHttp || result.Scheme == Uri.UriSchemeHttps))
                        {
                            fileName = lowName;
                        }
                        else
                        {
                            if(officialExtensions == null)
                                officialExtensions = FileSettings.Parse(client.DownloadString(args.GetValue("Repository-List")));
                            if(!officialExtensions.ContainsKey(lowName))
                                fileName = lowName;
                            else
                                if(!officialExtensions.TryGetValue(lowName + "___name", out fileName))
                                    fileName = lowName;
                        }

                        string dest = Path.Combine(args.GetValue("Extensions-Folder"), fileName.ToLower() + ".dll");
                        if(File.Exists(dest))
                        {
                            File.Delete(dest);
                            Console.Error.WriteLine(" [o] Uninstalled {0}.", name);
                        }
                        else
                            Console.Error.WriteLine(" [-] {0} was not installed.", name);
                    }
                }
                catch(Exception ex)
                {
                    Console.Error.WriteLine(" [!] Error while managing {0} : {1}.", name, ex.Message);
                }
            }
        }

        public static void InstallExtensions(
            string[] names,
            ConsoleManager.Arguments args)
        {
            if(!Directory.Exists(args.GetValue("Extensions-Folder")))
                Directory.CreateDirectory(args.GetValue("Extensions-Folder"));
            
            Dictionary<string, string> officialExtensions = null;

            foreach(string aname in names)
            {
                string name;
                if(aname == "-")
                {
                    Console.Write(" [?] Extension name to install : ");
                    name = Console.ReadLine();
                }
                else
                    name = aname;
                
                Console.Error.WriteLine(" [ ] Installing {0}...", name);
                try
                {
                    string lowName = name.ToLower();

                    string url;
                    string fileName;

                    using(var client = new WebClient())
                    {
                        Uri result;
                        if(Uri.TryCreate(name, UriKind.Absolute, out result) && (result.Scheme == Uri.UriSchemeHttp || result.Scheme == Uri.UriSchemeHttps))
                        {
                            url = name;
                            fileName = lowName;
                        }
                        else
                        {
                            if(officialExtensions == null)
                                officialExtensions = FileSettings.Parse(client.DownloadString(args.GetValue("Repository-List")));
                            if(!officialExtensions.ContainsKey(lowName))
                            {
                                Console.Error.WriteLine(" [!] Can't find {0} in the repository.", name);
                                continue;
                            }
                            url = officialExtensions[lowName];

                            if(!officialExtensions.TryGetValue(lowName + "___name", out fileName))
                                fileName = lowName;
                        }
                    
                        string tempFile = Path.GetTempFileName();
                        client.DownloadFile(url, tempFile);

                        string dest = Path.Combine(args.GetValue("Extensions-Folder"), fileName.ToLower() + ".dll");
                        if(File.Exists(dest))
                            File.Delete(dest);
                        File.Move(tempFile, dest);
                        
                        Console.Error.WriteLine(" [o] Installed {0}.", name);
                    }
                }
                catch(WebException)
                {
                    Console.Error.WriteLine(" [!] Can't find/download {0}.", name);
                }
                catch(Exception ex)
                {
                    Console.Error.WriteLine(" [!] Error while managing {0} : {1}.", name, ex.Message);
                }
            }
        }

        public static void Start(/*List<string> largs*/ConsoleManager.Arguments config)
        {
            bool error = false;
            if(!config.HasValue("Cloud-IP"))
            {
                Console.Error.WriteLine(" [!] You have to specify the ip/port.");
                error = true;
            }
            if(!config.HasValue("Files-Path"))
            {
                Console.Error.WriteLine(" [!] You have to specify the folder where to store the cloud files.");
                error = true;
            }
            if(error)
                return;

            SecureSessionManager = new SecureSessionManager(config);
            
            Console.WriteLine(" [ ] Starting...");
            IPEndPoint endPoint = CoreHelper.ParseIPEndPoint(config.GetValue("Cloud-IP"));
            
            new Thread(() => TCPStart(new IPEndPoint(endPoint.Address, endPoint.Port + 1)))
                .Start();
            
            Server(endPoint);
        }

        private static SecureSessionManager SecureSessionManager;
        
        private static void Server(IPEndPoint endPoint)
        {
            try
            {
                using(UdpClient server = new UdpClient(endPoint))
                {
                    Console.WriteLine(" [o] Started UDP on {0}", endPoint);

                    try
                    {
                        uint SIO_UDP_CONNRESET = 0x80000000 | 0x18000000 | 12;
                        server.Client.IOControl((int)SIO_UDP_CONNRESET, new byte[] { Convert.ToByte(false) }, null);
                    }
                    catch(System.Net.Sockets.SocketException)
                    { }

                    while(true)
                    {
                        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
                        byte[] data = server.Receive(ref sender);
                        
                        new Thread(() => ManageClient(server.Client, sender, data))
                            .Start();
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine(" [!] {0}", ex.Message);
                Console.WriteLine(" [i] Abort.");
            }
        }

        public static void TCPStart(IPEndPoint endPoint)
        {
            try
            {
                TcpListener server = new TcpListener(endPoint);
                server.Start();

                Console.WriteLine(" [o] Started TCP on {0}", endPoint);
                
                while(true)
                {
                    Socket client = server.AcceptSocket();
                    
                    new Thread(() => TCPManageClient(client))
                        .Start();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine(" [!] TCP : {0}", ex.Message);
                Console.WriteLine(" [i] TCP : Abort.");
            }
        }

        public static void TCPManageClient(Socket client)
        {
            using(var stream = new NetworkStream(client))
                SecureSessionManager.RouteStream(stream);
        }

        public static void ManageClient(Socket server, IPEndPoint sender, byte[] data)
        {
            try
            {
                Console.WriteLine(" [>] {0}", sender);

                data = SecureSessionManager.RouteRequest(sender, Encoding.UTF8.GetString(data));
                
                if(sender != null)
                    server.SendTo(data, data.Length, SocketFlags.None, sender);
                else
                    server.Send(data, data.Length, SocketFlags.None);
            }
            catch(System.Xml.XmlException)
            {
                string errorMessage = Session
                    .GetError(1, "Can't parse the request.")
                    .ToString(SaveOptions.DisableFormatting);
                data = Encoding.UTF8.GetBytes(errorMessage);
                if(sender != null)
                    server.SendTo(data, data.Length, SocketFlags.None, sender);
                else
                    server.Send(data, data.Length, SocketFlags.None);
            }
            catch(Exception ex)
            {
                Console.WriteLine(" [!] {0}", ex);
            }
        }
    }
}
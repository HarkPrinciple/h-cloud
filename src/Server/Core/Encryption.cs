using System.Collections.Concurrent;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Security;
using System.Numerics;
using System.Linq;
using System.Text;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public partial class CloudCore
    {
        public int NbDispatchMin
        {
            get;
            set;
        }

        public int NbDispatchSpan
        {
            get;
            set;
        }

        public int NbDispatchMax
        {
            get
            {
                return NbDispatchMin + NbDispatchSpan;
            }
        }

        protected IDictionary<User, List<CoreCache>> coreCache = new ConcurrentDictionary<User, List<CoreCache>>();
        protected IDictionary<User, DateTime> coreCacheTime = new ConcurrentDictionary<User, DateTime>();

        protected void GetKeyIV(
            string fileNameWithoutExtension,
            User user,
            out byte[] key,
            out byte[] iv)
        {
            List<CoreCache> cache;
            if(coreCache.TryGetValue(user, out cache))
            {
                coreCacheTime[user] = DateTime.Now;
                foreach(CoreCache c in cache)
                    if(c.Name == fileNameWithoutExtension)
                    {
                        key = c.Key;
                        iv = c.IV;
                        return;
                    }
            }
            else
            {
                cache = new List<CoreCache>();
                coreCacheTime[user] = DateTime.Now;
                coreCache[user] = cache;

                DateTime now = DateTime.Now;
                TimeSpan timeout = new TimeSpan(1, 0, 0);
                List<User> cleanUp = new List<User>();
                foreach(var kv in coreCacheTime)
                {
                    if(now - kv.Value > timeout)
                        cleanUp.Add(kv.Key);
                }
                foreach(User u in cleanUp)
                {
                    coreCache.Remove(u);
                    coreCacheTime.Remove(u);
                }
            }

            // PBKDF2
            using(Rfc2898DeriveBytes derive = new Rfc2898DeriveBytes(
                user.Password,
                (user.Name + fileNameWithoutExtension).GetBytes(),
                user.NbIterations))
            {
                key = derive.GetBytes(32);
                iv = derive.GetBytes(16);
            }

            cache.Add(new CoreCache()
            {
                Name = fileNameWithoutExtension,
                Key = key,
                IV = iv
            });
        }
        
        protected class CoreCache
        {
            public string Name
            {
                get;
                set;
            }

            public byte[] Key
            {
                get;
                set;
            }

            public byte[] IV
            {
                get;
                set;
            }
        }

        protected Stream CreateDispatchStream(string[] names, FileMode fileMode, User user)
        {
            Stream[] streams = new Stream[names.Length];
            for(int i = 0; i < names.Length; ++i)
            {
                string path = GetFilePath(names[i]);
                Stream cstream;
                if(fileMode == FileMode.Open)
                {
                    cstream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                    SoundStream.SkipSoundHeader(cstream);
                }
                else
                {
                    cstream = new SoundStream(File.Open(path, fileMode));
                }

                byte[] key, iv;
                GetKeyIV(names[i], user, out key, out iv);
                streams[i] = new AESStream(cstream, key, iv);
            }
            
            if(fileMode != FileMode.Open)
            {
                streams[0].Write(streams.Length.GetBytes());
                for(int i = 1; i < names.Length; ++i)
                    streams[0].WriteWrapped(names[i].GetBytes());
                streams[0].Flush();
            }
            else
            {
                int nb = streams[0].Read(4).ToInt32();
                for(int i = 1; i < nb; ++i)
                    streams[0].ReadWrapped();
            }
            
            return new DispatchStream(streams);
        }
        protected Stream CreateDispatchStream(string originPath, FileMode fileMode, User user)
        {
            string[] names = GetNames(originPath, user);

            return CreateDispatchStream(names, fileMode, user);
        }

        protected string[] GetNames(string originPath, User user)
        {
            string[] names;
            string fnwe = GetFileNameFromPath(originPath);

            byte[] key, iv;
            GetKeyIV(fnwe, user, out key, out iv);
            
            using(Stream fileStream = File.Open(originPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            using(Stream cryptedFileStream = new AESStream(fileStream, key, iv))
            {
                SoundStream.SkipSoundHeader(fileStream);

                names = new string[cryptedFileStream.Read(4).ToInt32()];
                
                names[0] = fnwe;
                for(int i = 1; i < names.Length; ++i)
                    names[i] = cryptedFileStream.ReadWrapped().GetString();
            }

            return names;
        }
        
        protected void AllocateFiles(string[] names, User user)
        {
            using(Stream multiStream = CreateDispatchStream(names, FileMode.Create, user))
            {
                multiStream.WriteByte((byte)Random.Next());

                for(int i = 0; i < names.Length; ++i)
                    multiStream.Write("No Data".GetBytes());
                
                multiStream.Flush();
            }
        }
    }
}
using System.Xml.XPath;
using System.Xml.Linq;
using System;

namespace Hark.Cloud.Client
{
    [System.Serializable]
    public class CloudException : System.Exception
    {
        public CloudException(XElement element)
            : this(int.Parse(element.XPathSelectElement("code").Value), element.XPathSelectElement("message").Value)
        { }
        public CloudException(int code, string message)
            : base(message)
        {
            this.Code = code;
        }

        public int Code
        {
            get;
            private set;
        }
    }
}
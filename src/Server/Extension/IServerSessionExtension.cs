using System.Collections.Concurrent;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public abstract class IServerSessionExtension : IDisposable
    {
        public abstract string CommandName
        {
            get;
        }

        public virtual bool IsStateful
        {
            get
            {
                return false;
            }
        }

        public virtual bool IsDownloadable
        {
            get
            {
                return true;
            }
        }

        private ConcurrentBag<Thread> _Threads = new ConcurrentBag<Thread>();
        public ConcurrentBag<Thread> Threads
        {
            get
            {
                return _Threads;
            }
        }

        public abstract XElement Compute(
            Session session,
            IPEndPoint sender,
            XElement doc);

        public virtual void Dispose()
        { }
    }
}
using System.Xml.Linq;
using System;

namespace Hark.Cloud.Client
{
    public partial class Client
    {
        public void ClaimAsAdmin()
        {
            Session.Send(new XElement("admin",
                new XElement("claim")
            ));

            Console.WriteLine(" [o] You are now admin.");
        }

        public void Shutdown()
        {
            Console.WriteLine(" [ ] Shutdown... (may timeout)");

            Session.Send(new XElement("admin",
                new XElement("shutdown")
            ));

            Console.WriteLine(" [o] Shutdown.");
        }
    }
}
# H-Cloud

## Description

This project is a portable cloud. On this cloud, you can :

* Store data/files
* Keep your data safe and untraceable by a thrid party
* Use extensions to :
    * Execute/Manager remote code (ex: a Web Site or a WebDav server)
    * Provide more features (ex: manage a set of cloud files with meta data)
    * Use the cloud as a software repository

This cloud is made of :

* A XML based protocol for easy integration in different software
* Secured sessions with :
    * RSA to transmit AES key
    * AES to transmit requests' content
    * Time out (configurable on connection)
    * Identifiable by an id and the AES key = you can share your session if you want to
    * User name and password to provide a owner for each file
    * No user database = better security
    * No owned files database = better security
    * No access to the files encrypted by others
* A folder with encrypted/splitted/anonymised files :
    * A file is splitted into a random number of cloud files
    * The list of cloud files representing the file is stored in the head cloud file
    * Every cloud file is encrypted on the fly, piece by piece, and can be decrypted on the fly
    * The write in a file is done turn by turn to each cloud file
    * Every cloud file is wrapped by a sound file header, for greater anonymity
    * If a user can't decrypt a file, it means he/she is not the owner
    * The head cloud file contains a random byte to optimize the detection if the user is the owner
* Extensions stateful or stateless which can :
    * Access the files of the user
    * Access the basic functionalities of the cloud core
    * Can be downloaded to improve the use of the command line client
* A command line client, which can :
    * Download extensions to provide more features
    * Connect to the cloud
    * Send queries to the cloud (server)
    * Upload and download files to the cloud

| | |
| --------|---------|
| :warning: | Caution on extensions. Use only thrust worthy extensions, with open source code. A malicious extension would be able to erase files or damage your machine. |

## Project structure

```
HarkLib
|:: .make               - Solution builder folder
    |:: config.ini      - Configuration file for compilation
|:: .vscode             - Visual Studio Code folder (optional)
|:: out                 - Output folder
|:: src                 - Source code
    |:: Client          - Client source code
    |:: Extension       - Example of an extension (useful for tests)
    |:: Server          - Server/Cloud source code
```

## Compilation

> If your aim is to use it and not compile it, you can use the files
in the folder `out`. They will work on Windows and UNIX-based operating
system supporting, for instance, Mono.

In order to compile the project, you need to have a C# compiler.

* On Windows, you can use `csc.exe` provided by Microsoft.
* On UNIX-based operating system, you can use `dmcs` from Mono
provided by Xamarin.

If you want to use the automatited compilation of the whole project
provided in this repository, you will need to execute the make file.

To do so, you need to have the C# compiler accessible from your
environment variable PATH and to have a "maker" (ex: GNU make).

Then, execute from the root folder the command `make` or from Visual
Studio Code the keyboard shortcut CTRL+SHIFT+B.

> Note : If you compile on Windows, in most cases, you will have no need
to compile it again to make it work on another operating system/machine.

## Execution

In order to run the cloud, execute the following command :
```
# UNIX-based operating system
mono HarkCloud.exe

# Windows
HarkCloud.exe
```

In order to run the client, execute the following command :
```
# UNIX-based operating system
mono HarkCloud.Client.exe <args>

# Windows
HarkCloud.Client.exe <args>
```

It is recommended to create an alias/macro to improve the feeling
of the use of the client.

On UNIX-based operating systems, create a file `<alias>` accessible
from your PATH, with execution right and put the following content inside :
```
#!/bin/bash
mono HarkCloud.Client.exe "$@"
```

On Windows, create a file `<alias>.cmd` accessible from your PATH and put
the following content inside :
```
@echo off
HarkCloud.Client.exe %*
```

## Dependencies

It uses the libraries `HarkLib.Core` and `HarkLib.Security`.

## License

The project is licensed with FreeBSD License (2-clause license).

[ [Can-Cannot-Must description](https://www.tldrlegal.com/l/freebsd) ] | [ [Learn more](https://opensource.org/licenses/BSD-2-Clause) ]

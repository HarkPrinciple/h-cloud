using System.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public class SessionExtension : IDisposable
    {
        public SessionExtension(Type type, string filePath)
        {
            this.FilePath = filePath;
            this.Instance = null;
            this.Type = type;
        }

        public string FilePath
        {
            get;
            private set;
        }

        public Type Type
        {
            get;
            private set;
        }

        protected IServerSessionExtension CreateInstance()
        {
            return (IServerSessionExtension)Activator.CreateInstance(Type);
        }

        public string CommandName
        {
            get
            {
                return PropertyInstance.CommandName
                    .Trim()
                    .ToLower();
            }
        }

        public bool IsStateful
        {
            get
            {
                return PropertyInstance.IsStateful;
            }
        }

        public bool IsDownloadable
        {
            get
            {
                return PropertyInstance.IsDownloadable;
            }
        }

        private IServerSessionExtension _PropertyInstance = null;
        private IServerSessionExtension PropertyInstance
        {
            get
            {
                if(_PropertyInstance == null)
                {
                    if(Instance != null)
                    {
                        _PropertyInstance = Instance;
                    }
                    else
                    {
                        _PropertyInstance = CreateInstance();
                        if(_PropertyInstance.IsStateful)
                            Instance = _PropertyInstance;
                    }
                }
                return _PropertyInstance;
            }
        }
        private IServerSessionExtension Instance
        {
            get;
            set;
        }

        protected IServerSessionExtension GetInstance()
        {
            if(Instance == null || !IsStateful)
                Instance = CreateInstance();

            return Instance;
        }

        public XElement Compute(
            Session session,
            IPEndPoint sender,
            XElement doc,
            out IServerSessionExtension instance)
        {
            instance = GetInstance();

            return instance.Compute(
                session,
                sender,
                doc
            );
        }

        public void Dispose()
        {
            GetInstance().Dispose();
        }
    }
}
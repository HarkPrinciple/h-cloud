using System.Security.Cryptography;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Numerics;
using System.Security;
using System.Text;
using System.Linq;
using System.Net;
using System.IO;
using System;

using HarkLib.Security;
using HarkLib.Core;

namespace Hark.Cloud.Server
{
    public partial class Session : IDisposable
    {
        public Session(
            CloudCore cloudCore,
            byte[] aesKey,
            byte[] aesIV,
            User user,
            int timeout,
            int globalTimeout,
            IDictionary<string, SessionExtension> extensions)
        {
            this.RunningExtensionInstances = new List<IServerSessionExtension>();
            this.GlobalTimeout = globalTimeout;
            this.CreationDate = DateTime.Now;
            this.LastRequest = DateTime.Now;
            this.Extensions = extensions;
            this.CloudCore = cloudCore;
            this.StreamId = 0;
            this.Streams = new ConcurrentDictionary<string, StreamInfo>();
            this.Timeout = timeout;
            this.AESKey = aesKey;
            this.AESIV = aesIV;
            this.Alive = true;
            this.User = user;
        }

        public IDictionary<string, StreamInfo> Streams
        {
            get;
            private set;
        }

        public int StreamId
        {
            get;
            private set;
        }

        public CloudCore CloudCore
        {
            get;
            private set;
        }

        public byte[] AESKey
        {
            get;
            set;
        }

        public byte[] AESIV
        {
            get;
            set;
        }
        
        public User User
        {
            get;
            set;
        }

        public DateTime CreationDate
        {
            get;
            private set;
        }

        public int GlobalTimeout
        {
            get;
            set;
        }

        public DateTime LastRequest
        {
            get;
            private set;
        }

        public int Timeout
        {
            get;
            set;
        }

        public bool TimedOut
        {
            get
            {
                DateTime now = DateTime.Now;
                return now > LastRequest.AddSeconds(Timeout)
                    || (GlobalTimeout > 0 && now > CreationDate.AddSeconds(GlobalTimeout));
            }
        }

        public bool Alive
        {
            get;
            private set;
        }

        public IDictionary<string, SessionExtension> Extensions
        {
            get;
            private set;
        }

        private List<IServerSessionExtension> RunningExtensionInstances
        {
            get;
            set;
        }

        private string AdminClaimName
        {
            get
            {
                return ".admin";
            }
        }

        public bool IsAdmin()
        {
            string path = CloudCore.GetAliasFilePath(AdminClaimName, User.UniversalUser);
            if(!File.Exists(path))
                return false;
            
            return CloudCore.IsOwner(path, User);
        }
        public void ThrowIfNotAdmin(string message = null)
        {
            if(!IsAdmin())
                throw new CloudMessageException(message ?? "You don't have the right to execute this command.");
        }

        public void Dispose()
        {
            foreach(var str in Streams.Values)
                str.Stream.Close();

            foreach(var instance in RunningExtensionInstances)
            {
                try
                {
                    instance.Dispose();
                }
                catch
                { }
            }
        }

        public static XElement GetError(int code, string message)
        {
            return new XElement("error",
                new XElement("code", code),
                new XElement("message", message)
            );
        }

        public static string GetOrThrow(XElement doc, string xpath)
        {
            XElement el = doc.XPathSelectElement(xpath);
            if(el == null)
                throw new CloudMessageException(1, "Can't find tag matching XPath \"" + xpath + "\".");
            
            return el.Value;
        }

        public static string GetOr(XElement doc, string xpath, string defaultValue)
        {
            XElement el = doc.XPathSelectElement(xpath);
            if(el == null)
                return defaultValue;
            
            return el.Value;
        }

        private string GetFilePath(XElement doc, out XElement tag)
        {
            XElement eid = doc.XPathSelectElement("/file/id");
            XElement ealias = doc.XPathSelectElement("/file/alias");
            if(eid == null && ealias == null)
                throw new CloudMessageException(1, "Can't find a file id tag or a file alias tag.");

            bool isAlias = eid == null;

            string idAlias = isAlias ? ealias.Value : eid.Value;
            tag = new XElement(isAlias ? "alias" : "id", idAlias);

            if(isAlias)
                return CloudCore.GetAliasFilePath(idAlias, User);
            else
                return CloudCore.GetFilePath(idAlias);
        }

        public CloudFile CloudFileFromId(string id = null)
        {
            return CloudFile.FromId(id, User, CloudCore);
        }
        public CloudFile CloudFileFromAlias(string alias, User nameSharing = null, User owner = null)
        {
            return CloudFile.FromAlias(alias, nameSharing ?? User, CloudCore, owner ?? User);
        }
    }
}